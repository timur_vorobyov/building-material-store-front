import PrivateRoute from 'components/PrivateRoute/PrivateRoute';
import FavoritePage from 'pages/FavoriteProductPage/FavoriteProductPage';
import HomePage from 'pages/HomePage/HomePage';
import MyProductsPage from 'pages/MyProductsPage/MyProductsPage';
import OrderPage from 'pages/OrderPage/OrderPage';
import ProductPage from 'pages/ProductPage/ProductPage';
import UserOrderPage from 'pages/UserOrderPage/UserOrderPage';
import { RouteObject, useRoutes } from 'react-router-dom';
import { routes as routesConst } from 'utils/consts/route.consts';

export const routes: RouteObject[] = [
  {
    path: routesConst.home,
    element: <HomePage />
  },
  {
    path: routesConst.order,
    element: (
      <PrivateRoute>
        <OrderPage />
      </PrivateRoute>
    )
  },
  {
    path: routesConst.favoriteProduct,
    element: (
      <PrivateRoute>
        <FavoritePage />
      </PrivateRoute>
    )
  },
  {
    path: routesConst.myProductsList,
    element: (
      <PrivateRoute>
        <MyProductsPage />
      </PrivateRoute>
    )
  },
  {
    path: routesConst.userOrder,
    element: (
      <PrivateRoute>
        <UserOrderPage />
      </PrivateRoute>
    )
  },
  {
    path: routesConst.product,
    element: <ProductPage />
  }
];

export default function Router() {
  return useRoutes(routes);
}
