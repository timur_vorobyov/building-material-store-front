import { Context, createContext } from 'react';
import UserService from '@services/user/user.service';

const UserServiceContext: Context<UserService> = createContext({} as UserService);

export default UserServiceContext;
