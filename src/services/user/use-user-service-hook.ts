import { useContext } from 'react';
import UsersService from './user.service';
import UsersServiceContext from './user.service.context';

export const useUserService = () => {
  const context = useContext<UsersService>(UsersServiceContext);
  if (Object.keys(context).length === 0) {
    throw new Error(
      'UsersServiceContext was not provided. Make sure your component is a part of UsersServiceProvider'
    );
  }
  return context;
};
