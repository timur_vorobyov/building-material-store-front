import BaseService from '@services/base.service';
import UserInterface from '@services/user/interface/user.interface';
import CreateUserDto from '@services/user/dto/create-user.dto';
import QueryUserDto from './dto/query-user.dto';

export default class UserService extends BaseService<UserInterface, CreateUserDto, QueryUserDto> {
  baseEndpoint = 'user';
}
