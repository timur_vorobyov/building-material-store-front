import BaseInterface from '@services/base.interface';
import { ProductInterface } from '@services/product/interface/product.interface';
import { CartInterface } from '@services/product/product-cart/interface/product-cart.interface';

export default interface UserInterface extends BaseInterface {
  id: number;
  isAdmin: boolean;
  phone: string;
  name: string;
  favorite: ProductInterface[];
  cart: CartInterface[];
}
