import UsersService from '@services/user/user.service';
import { FC, PropsWithChildren } from 'react';
import UserServiceContext from '@services/user/user.service.context';

const UserServiceProvider: FC<PropsWithChildren> = ({ children }) => {
  const createdUserService = new UsersService();
  return (
    <UserServiceContext.Provider value={createdUserService}>{children}</UserServiceContext.Provider>
  );
};

export default UserServiceProvider;
