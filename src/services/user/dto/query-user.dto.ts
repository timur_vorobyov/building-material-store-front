import BaseQueryDto from '@services/base-query.dto';

export default interface QueryUserDto extends BaseQueryDto {
  name: string;
}
