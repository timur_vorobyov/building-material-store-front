export default interface CreateUserDto {
  role: 'admin';
  phone?: string;
  name: string;
  email: string;
}
