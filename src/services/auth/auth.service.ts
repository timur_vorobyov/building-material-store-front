import LoginDto from '@contexts/auth/dto/login.dto';
import SignUpDto from '@contexts/auth/dto/sign-up.dto';
import BaseService from '@services/base.service';
import UserInterface from '@services/user/interface/user.interface';
import { AxiosResponse } from 'axios';

export default class AuthService {
  baseEndpoint = 'auth';

  async sendOtpLoginCode(data: Omit<LoginDto, 'otpCode'>): Promise<{ data: string }> {
    return BaseService.request({
      method: 'post',
      url: `${this.baseEndpoint}/send-verify-code-login`,
      data
    });
  }

  async sendOtpSignUpCode(phone: string): Promise<{ data: string }> {
    return BaseService.request({
      method: 'post',
      url: `${this.baseEndpoint}/send-verify-code-sign-up`,
      data: { phone }
    });
  }

  async signUp(data: SignUpDto): Promise<{
    expiresIn?: number;
    accessToken?: string;
    userData?: UserInterface;
  }> {
    const response = await BaseService.request<AxiosResponse>({
      method: 'post',
      url: `${this.baseEndpoint}/sign-up`,
      data
    });
    return response.data;
  }

  async login(data: LoginDto): Promise<{ accessToken?: string; expiresIn?: number }> {
    const response = await BaseService.request<AxiosResponse>({
      method: 'post',
      url: `${this.baseEndpoint}/login`,
      data
    });
    return response.data;
  }
}
