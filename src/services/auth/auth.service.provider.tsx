import { FC, PropsWithChildren } from 'react';
import AuthService from '@services/auth/auth.service';
import AuthServiceContext from './auth.service.context';

const AuthServiceProvider: FC<PropsWithChildren> = ({ children }) => {
  const createdAuthService = new AuthService();
  return (
    <AuthServiceContext.Provider value={createdAuthService}>{children}</AuthServiceContext.Provider>
  );
};

export default AuthServiceProvider;
