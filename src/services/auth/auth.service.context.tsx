import { Context, createContext } from 'react';
import AuthService from './auth.service';

const AuthServiceContext: Context<AuthService> = createContext({} as AuthService);

export default AuthServiceContext;
