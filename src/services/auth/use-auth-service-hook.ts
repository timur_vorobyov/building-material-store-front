import { useContext } from 'react';
import AuthService from '@services/auth/auth.service';
import AuthServiceContext from '@services/auth/auth.service.context';

export const useAuthService = () => {
  const context = useContext<AuthService>(AuthServiceContext);
  if (Object.keys(context).length === 0) {
    throw new Error(
      'AuthServiceContext was not provided. Make sure your component is a part of AuthServiceProvider'
    );
  }
  return context;
};
