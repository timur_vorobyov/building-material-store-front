import {
  deleteCookie,
  deleteLocalStorage,
  getCookie,
  getLocalStorage,
  setLocalStorage
} from '@helpers/storage.helper';
import axiosInstance from '@services/api';
import UserInterface from '@services/user/interface/user.interface';

export const getUserDataHelper = (): UserInterface | undefined => {
  try {
    return getLocalStorage('user') as UserInterface;
  } catch (e) {
    return undefined;
  }
};
export const setUserDataHelper = (userData: UserInterface | undefined) =>
  userData && setLocalStorage('user', JSON.stringify(userData));
export const delUserDataHelper = () => deleteLocalStorage('user');
export const getCookieUserHelper = (): { id: number | string } | undefined =>
  getCookie('u') as { id: number | string };
export const resetAuthHelper = async () => {
  axiosInstance.post('auth/log-out').then(() => {
    delUserDataHelper();
    deleteCookie('u');
  });
};
