import { FC, PropsWithChildren } from 'react';
import UsersServiceProvider from '@services/user/user.service.provider';
import AuthServiceProvider from './auth/auth.service.provider';
import ProductServiceProvider from './product/product.service.provider';
import OrderServiceProvider from './order/order.service.provider';

const GlobalServices: FC<PropsWithChildren> = ({ children }) => {
  return (
    <UsersServiceProvider>
      <AuthServiceProvider>
        <ProductServiceProvider>
          <OrderServiceProvider>{children}</OrderServiceProvider>
        </ProductServiceProvider>
      </AuthServiceProvider>
    </UsersServiceProvider>
  );
};

export default GlobalServices;
