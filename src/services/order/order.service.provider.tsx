import { FC, PropsWithChildren } from 'react';
import OrderService from './order.service';
import OrderServiceContext from './order.service.context';

const OrderServiceProvider: FC<PropsWithChildren> = ({ children }) => {
  const createdOrderService = new OrderService();
  return (
    <OrderServiceContext.Provider value={createdOrderService}>
      {children}
    </OrderServiceContext.Provider>
  );
};

export default OrderServiceProvider;
