import { Context, createContext } from 'react';
import OrderService from './order.service';

const OrderServiceContext: Context<OrderService> = createContext({} as OrderService);

export default OrderServiceContext;
