import BaseService from '@services/base.service';
import { OrderInterface } from './interface/order.interface';
import { QueryOrderDto } from './dto/query-order.dto';
import { CreateOrderDto } from './dto/create-order.dto';
import ServerResponseInterface from 'utils/types/server-response.interface';

export default class OrderService extends BaseService<
  OrderInterface,
  CreateOrderDto,
  QueryOrderDto
> {
  baseEndpoint = 'order';

  async listOrders(params?: QueryOrderDto): Promise<OrderInterface[]> {
    return BaseService.request({
      method: 'get',
      url: this.baseEndpoint,
      params,
      transformResponse: (r: string) => {
        try {
          const resp = JSON.parse(r) as ServerResponseInterface<OrderInterface[]>;
          return resp.data;
        } catch (e) {
          return r;
        }
      }
    });
  }
}
