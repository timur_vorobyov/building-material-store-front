import BaseQueryDto from '@services/base-query.dto';
export interface QueryOrderDto extends BaseQueryDto {
  userId: number;
  status: boolean;
}
