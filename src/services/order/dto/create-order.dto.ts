export interface CreateOrderDto {
  userId: number;
  fullPrice: number;
}
