import { useContext } from 'react';
import OrderService from './order.service';
import OrderServiceContext from './order.service.context';

export const useOrderService = () => {
  const context = useContext<OrderService>(OrderServiceContext);
  if (Object.keys(context).length === 0) {
    throw new Error(
      'OrderServiceContext was not provided. Make sure your component is a part of OrderServiceProvider'
    );
  }
  return context;
};
