import BaseInterface from '@services/base.interface';
import { ProductInterface } from '@services/product/interface/product.interface';

export interface OrderInterface extends BaseInterface {
  id: number;
  fullPrice: number;
  location: string;
  coordinates: { lat: string; long: string };
  status: boolean;
  products: ProductInterface[];
}
