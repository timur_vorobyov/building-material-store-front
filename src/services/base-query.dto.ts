export default interface BaseQueryDto {
  take?: number;
  skip?: number;
}
