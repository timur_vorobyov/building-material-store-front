import { API_URL, NODE_ENV } from '@config';
import { AxiosError, AxiosRequestConfig } from 'axios';
import ServerResponseInterface from 'utils/types/server-response.interface';
import axiosInstance from './api';

export default class BaseService<I, SD, QP> {
  baseEndpoint = '';

  static request<T>(opts: AxiosRequestConfig): Promise<T> {
    try {
      return axiosInstance.request({
        baseURL: API_URL,
        transformResponse: (r: string) => {
          try {
            const parsed = JSON.parse(r) as ServerResponseInterface<T>;
            return parsed;
          } catch (e) {
            if (NODE_ENV !== 'production') console.error({ e, r });
            return r;
          }
        },
        ...opts
      });
    } catch (e) {
      if (NODE_ENV !== 'production') console.error(e);
      throw BaseService.handleError(e as AxiosError);
    }
  }

  static handleError(e: Error | AxiosError<ServerResponseInterface<unknown>>) {
    if (e instanceof AxiosError) {
      let errorMsg = e.message;

      if (e.response && e.response.data?.message) {
        errorMsg = e.response.data.message;
      }

      if (e.response?.data?.data?.length) {
        e.response.data.data.forEach(({ msg, param }: { msg: string; param: string }) => {
          errorMsg += `. ${msg}. (${param})`;
        });
      }

      e.message = errorMsg;
    }
    return e;
  }

  async baseSave(id: string | number | undefined, data: SD): Promise<{ data: I }> {
    if (typeof id === 'undefined') throw new Error('no id');
    return BaseService.request({
      method: 'patch',
      url: `${this.baseEndpoint}/${id}`,
      data
    });
  }

  async baseCreate(data: SD): Promise<{ data: I }> {
    return BaseService.request({
      method: 'post',
      url: `${this.baseEndpoint}`,
      data
    });
  }

  single(id: string | number | undefined, opts?: AxiosRequestConfig): Promise<{ data: I }> {
    if (typeof id === 'undefined') throw new Error('no id');
    return BaseService.request({
      method: 'get',
      url: `${this.baseEndpoint}/${id}`,
      ...opts
    });
  }

  async list(params?: QP): Promise<[I[], number]> {
    return BaseService.request({
      method: 'get',
      url: this.baseEndpoint,
      params,
      transformResponse: (r: string) => {
        try {
          const resp = JSON.parse(r) as ServerResponseInterface<I[]>;
          return [resp.data, resp.total];
        } catch (e) {
          return r;
        }
      }
    });
  }

  async save(data: SD, id?: number | string | undefined | null): Promise<{ data: I }> {
    const clone = { ...data };
    if (id && id !== '0') {
      return this.baseSave(id, clone);
    }
    return this.baseCreate(clone);
  }

  delete(id: string | number | undefined): Promise<I> {
    if (typeof id === 'undefined') throw new Error('no id');
    return BaseService.request({
      method: 'delete',
      url: `${this.baseEndpoint}/${id}`
    });
  }
}
