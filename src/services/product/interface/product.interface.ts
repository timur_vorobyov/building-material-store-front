import BaseInterface from '@services/base.interface';

export interface ProductInterface extends BaseInterface {
  id: number;
  price: number;
  nameRu: string;
  nameEn: string;
  nameUz: string;
  descRu: string;
  descEn: string;
  descUz: string;
  imagesUrls: string[];
  category: {
    nameRu: string;
  };
  brand: {
    name: string;
  };
}
