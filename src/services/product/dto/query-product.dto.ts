import BaseQueryDto from '@services/base-query.dto';
export interface QueryProductDto extends BaseQueryDto {
  price?: number[];
  userId?: number;
  productsIds?: number[];
}
