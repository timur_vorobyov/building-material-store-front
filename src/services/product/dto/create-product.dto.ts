export interface CreateProductDto {
  userId: number;
  price: number;
  title: string;
  desc: string;
  imageUrls: string[];
}
