import { useContext } from 'react';
import ProductService from './product.service';
import ProductServiceContext from './product.service.context';

export const useProductService = () => {
  const context = useContext<ProductService>(ProductServiceContext);
  if (Object.keys(context).length === 0) {
    throw new Error(
      'ProductServiceContext was not provided. Make sure your component is a part of ProductServiceProvider'
    );
  }
  return context;
};
