import { ProductInterface } from '@services/product/interface/product.interface';

export interface CartInterface {
  id: number;
  product: ProductInterface;
  count: number;
}
