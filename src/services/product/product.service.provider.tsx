import { FC, PropsWithChildren } from 'react';
import ProductService from './product.service';
import ProductServiceContext from './product.service.context';

const ProductServiceProvider: FC<PropsWithChildren> = ({ children }) => {
  const createdProductService = new ProductService();
  return (
    <ProductServiceContext.Provider value={createdProductService}>
      {children}
    </ProductServiceContext.Provider>
  );
};

export default ProductServiceProvider;
