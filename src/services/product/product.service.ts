import BaseService from '@services/base.service';
import ServerResponseInterface from 'utils/types/server-response.interface';
import { CreateProductDto } from './dto/create-product.dto';
import { QueryFavoriteProductDto } from './dto/query-favorite-product.dto';
import { QueryProductDto } from './dto/query-product.dto';
import { ProductInterface } from './interface/product.interface';
import { CartInterface } from './product-cart/interface/product-cart.interface';

export default class ProductService extends BaseService<
  ProductInterface,
  CreateProductDto,
  QueryProductDto
> {
  baseEndpoint = 'product';

  addToFavorite(data: { userId: number; productId: number }): Promise<{ data: ProductInterface }> {
    return BaseService.request({
      method: 'post',
      data,
      url: `${this.baseEndpoint}/favorite`
    });
  }

  removeFromFavorite(params: {
    userId: number;
    productId: number;
  }): Promise<{ data: ProductInterface }> {
    return BaseService.request({
      method: 'delete',
      params,
      url: `${this.baseEndpoint}/favorite`
    });
  }

  addToCart(data: { userId: number; productId: number }): Promise<{ data: CartInterface }> {
    return BaseService.request({
      method: 'post',
      data,
      url: `${this.baseEndpoint}/cart`
    });
  }

  removeFromCart(params: { userId: number; productId: number }): Promise<{ data: CartInterface }> {
    return BaseService.request({
      method: 'delete',
      params,
      url: `${this.baseEndpoint}/cart`
    });
  }

  listFromCart(params?: QueryProductDto | undefined): Promise<CartInterface[]> {
    return BaseService.request({
      method: 'get',
      url: `${this.baseEndpoint}/cart`,
      params,
      transformResponse: (r: string) => {
        try {
          const resp = JSON.parse(r) as ServerResponseInterface<CartInterface[]>;
          return resp.data ? resp.data : [];
        } catch (e) {
          return r;
        }
      }
    });
  }

  listFavorite(params?: QueryFavoriteProductDto | undefined): Promise<ProductInterface[]> {
    return BaseService.request({
      method: 'get',
      url: `${this.baseEndpoint}/favorite`,
      params,
      transformResponse: (r: string) => {
        try {
          const resp = JSON.parse(r) as ServerResponseInterface<ProductInterface[]>;
          return resp.data;
        } catch (e) {
          return r;
        }
      }
    });
  }

  myProductsList(params: QueryFavoriteProductDto): Promise<ProductInterface[]> {
    return BaseService.request({
      method: 'get',
      url: `${this.baseEndpoint}/owner-list`,
      params,
      transformResponse: (r: string) => {
        try {
          const resp = JSON.parse(r) as ServerResponseInterface<ProductInterface[]>;
          return resp.data;
        } catch (e) {
          return r;
        }
      }
    });
  }
}
