import { Context, createContext } from 'react';
import ProductService from './product.service';

const ProductServiceContext: Context<ProductService> = createContext({} as ProductService);

export default ProductServiceContext;
