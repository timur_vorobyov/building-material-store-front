const LeftArrowIcon = () => {
  return (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M6.81299 12.6121L1.5316 7.33068C1.34897 7.14804 1.34897 6.85192 1.5316 6.66929L6.81299 1.3879" />
    </svg>
  );
};

export default LeftArrowIcon;
