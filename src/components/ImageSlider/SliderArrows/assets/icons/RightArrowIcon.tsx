const RightArrowIcon = () => {
  return (
    <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1.18701 12.6121L6.4684 7.33068C6.65103 7.14804 6.65103 6.85192 6.4684 6.66929L1.18701 1.3879" />
    </svg>
  );
};

export default RightArrowIcon;
