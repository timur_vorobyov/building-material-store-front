import LeftArrowIcon from './assets/icons/LeftArrowIcon';
import RightArrowIcon from './assets/icons/RightArrowIcon';
import cn from 'clsx';
import { useSwiper } from 'swiper/react';
import { useEffect, useState } from 'react';
import Button from 'components/ui-kit/Button/Button';

const SliderArrows = () => {
  const [slideConfig, setSlideConfig] = useState({
    isBeginning: true,
    isEnd: false
  });
  const swiper = useSwiper();

  useEffect(() => {
    swiper.on('slideChange', (swipe) => {
      setSlideConfig({ isBeginning: swipe.isBeginning, isEnd: swipe.isEnd });
    });
  }, [swiper]);

  return (
    <>
      <Button
        onClick={() => swiper.slidePrev()}
        className={cn({ ['hide']: slideConfig.isBeginning }, 'arrow left')}>
        <LeftArrowIcon />
      </Button>
      <Button
        onClick={() => swiper.slideNext()}
        className={cn({ ['hide']: slideConfig.isEnd }, 'arrow right')}>
        <RightArrowIcon />
      </Button>
    </>
  );
};

export default SliderArrows;
