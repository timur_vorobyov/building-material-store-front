import { FC } from 'react';
import './ImageSlider.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import { A11y, Navigation, Pagination } from 'swiper';
interface ImageSliderProps {
  images: string[];
}
import 'swiper/css';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import SliderArrows from './SliderArrows/SliderArrows';
import { APP_MEDIA_URL } from '@config';

const ImageSlider: FC<ImageSliderProps> = ({ images }) => {
  return (
    <div className="image-slider">
      <Swiper
        modules={[Navigation, Pagination, A11y]}
        slidesPerView={1}
        navigation
        pagination={{ clickable: true }}
        className="mySwiper">
        <SliderArrows />
        {images.map((_, index) => (
          <SwiperSlide key={index}>
            <img src={APP_MEDIA_URL + images[index]} />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default ImageSlider;
