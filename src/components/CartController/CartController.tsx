import Button from 'components/ui-kit/Button/Button';
import Typography from 'components/ui-kit/Typography/Typography';
import MinusIcon from 'components/ProductsList/Product/assets/icons/MinusIcon';
import PlusIcon from 'components/ProductsList/Product/assets/icons/PlusIcon';
import { FC } from 'react';
import './CartController.scss';
import FavoriteIcon from 'components/ProductsList/Product/assets/icons/FavoriteIcon';
import translate from 'i18n/translate';
import { useAuth } from '@contexts/auth/use-auth-hook';
import { useProductService } from '@services/product/use-product-service-hook';
import { useMutation } from 'react-query';
import { ProductInterface } from '@services/product/interface/product.interface';
import { useAuthModal } from '@contexts/auth-modal/use-auth-modal-hook';
import { useLocation } from 'react-router-dom';
import { routes } from '@consts/route.consts';
interface CartControllerProps {
  product: ProductInterface;
}
const CartController: FC<CartControllerProps> = ({ product }) => {
  const { cart, setCart, user } = useAuth();
  const service = useProductService();
  const { setAuthModalData } = useAuthModal();
  const location = useLocation();

  const { mutate: addToCart } = useMutation({
    mutationKey: 'addToCart',
    mutationFn: (params: { userId: number; productId: number }) => service.addToCart(params),
    onSuccess: (cartData) => {
      const response = cartData.data;
      const cartItemIndex = cart.findIndex((item) => item.product.id === cartData.data.product.id);

      if (cartItemIndex === -1) {
        setCart([...cart, response]);
      } else {
        const updatedCart = [...cart];
        updatedCart[cartItemIndex].count = response.count;
        setCart(updatedCart);
      }
    }
  });

  const { mutate: removeFromCart } = useMutation({
    mutationKey: 'removeFromCart',
    mutationFn: (params: { userId: number; productId: number }) => service.removeFromCart(params),
    onSuccess: (cartData) => {
      const response = cartData.data;
      const cartItemIndex = cart.findIndex((item) => item.product.id === cartData.data.product.id);

      if (cartItemIndex !== -1) {
        let updatedCart = [...cart];

        if (cart[cartItemIndex].count - 1 === 0) {
          updatedCart = updatedCart.filter((c) => c.product.id !== response.product.id);
        } else {
          updatedCart[cartItemIndex].count = response.count;
        }

        setCart(updatedCart);
      }
    }
  });

  const handleAddToCart = () => {
    if (!user) {
      setAuthModalData({
        isShown: true,
        isSignUp: false
      });
    } else {
      const params = {
        userId: user.id,
        productId: product.id
      };

      addToCart(params);
    }
  };

  const handleRemoveFromCart = () => {
    const params = {
      userId: user!.id,
      productId: product.id
    };

    removeFromCart(params);
  };
  const cartItemIndex = cart.findIndex((item) => item.product.id === product.id);

  if (location.pathname === routes.myProductsList) return <></>;
  return (
    <div className="cart-controller">
      {cartItemIndex !== -1 ? (
        <>
          <Button
            className="modifier"
            onClick={(e) => {
              e.stopPropagation();
              handleRemoveFromCart();
            }}>
            <MinusIcon />
          </Button>
          <Typography variant="span">{cart[cartItemIndex].count}</Typography>
          <Button
            className="modifier"
            onClick={(e) => {
              e.stopPropagation();
              handleAddToCart();
            }}>
            <PlusIcon />
          </Button>
        </>
      ) : (
        <Button
          variant="button-4"
          className="in-cart-btn"
          onClick={(e) => {
            e.stopPropagation();
            handleAddToCart();
          }}>
          <FavoriteIcon />
          <Typography variant="span" fontWeight="semi-bold" fontSize={18}>
            {translate('inCart')}
          </Typography>
        </Button>
      )}
    </div>
  );
};

export default CartController;
