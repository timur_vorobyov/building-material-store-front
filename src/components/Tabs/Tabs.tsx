import './Tabs.scss';
import cn from 'clsx';
import { FC } from 'react';
import Typography from 'components/ui-kit/Typography/Typography';
import { StageNumbersEnum } from 'pages/OrderPage/enums';
import Button from 'components/ui-kit/Button/Button';

interface TabsProps {
  currentStage: StageNumbersEnum;
  setCurrentStage: (currentStage: StageNumbersEnum) => void;
  firstTitle: JSX.Element;
  secondTitle: JSX.Element;
}

const Tabs: FC<TabsProps> = ({ currentStage, setCurrentStage, firstTitle, secondTitle }) => {
  return (
    <div className="tabs">
      <Button
        className={cn({ ['active']: currentStage === StageNumbersEnum.Cart })}
        onClick={() => setCurrentStage(StageNumbersEnum.Cart)}>
        <div>
          <Typography fontSize={14} fontWeight="bold" variant="span" color="black">
            {firstTitle}
          </Typography>
        </div>
      </Button>

      <Button
        className={cn({ ['active']: currentStage === StageNumbersEnum.Order })}
        onClick={() => setCurrentStage(StageNumbersEnum.Order)}>
        <div>
          <Typography fontSize={14} fontWeight="bold" variant="span" color="black">
            {secondTitle}
          </Typography>
        </div>
      </Button>
    </div>
  );
};

export default Tabs;
