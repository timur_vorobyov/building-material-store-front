import { forwardRef } from 'react';
import InputMask, { ReactInputMask } from 'react-input-mask';
import './InputPhone.scss';
import { FieldError } from 'react-hook-form';
import Typography from '../Typography/Typography';

interface InputPhoneProps {
  error?: FieldError;
  onChange: (value: string) => unknown;
}

const InputPhone = forwardRef<ReactInputMask, InputPhoneProps>(({ onChange }, ref) => {
  return (
    <div className="input-phone">
      <Typography variant="label" fontWeight="bold">
        Телефон
      </Typography>
      <InputMask
        ref={ref}
        onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
          onChange(event.target.value.replace(/\s/g, '').replace(/_.*$/, '').substring(6));
        }}
        mask="(+\9\98) 99 999 99 99"
        maskChar={'_'}
        alwaysShowMask={true}
      />
    </div>
  );
});

export default InputPhone;
