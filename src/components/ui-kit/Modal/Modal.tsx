import './Modal.scss';
import { FC, ReactNode } from 'react';
import Button from '../Button/Button';
import CrossIcon from './assets/icons/CrossIcon';
import cn from 'clsx';
import Typography from '../Typography/Typography';

interface ModalProps {
  isGeo?: boolean;
  title: string;
  isModalShown: boolean;
  handleHideModal: () => void;
  body: ReactNode;
  footer?: ReactNode;
  width?: 'min' | 'max';
}
const Modal: FC<ModalProps> = ({
  isGeo,
  isModalShown,
  handleHideModal,
  title,
  body,
  footer,
  width = 'max'
}) => {
  if (!isModalShown) return null;

  return (
    <>
      <div
        className="modal-background"
        onClick={() => {
          handleHideModal();
        }}
      />
      <div
        className={cn(
          { ['min-wdith']: width === 'min', ['max-wdith']: width === 'max', ['geo']: isGeo },
          'modal-container'
        )}>
        <div className="modal-header-container">
          <div className="modal-header">
            <Button
              onClick={() => {
                handleHideModal();
              }}>
              <CrossIcon />
            </Button>
            <Typography variant="span" fontWeight="bold">
              {title}
            </Typography>
          </div>
        </div>
        <div className="modal-body">{body}</div>
        {footer ? <div className="modal-footer">{footer}</div> : null}
      </div>
    </>
  );
};

export default Modal;
