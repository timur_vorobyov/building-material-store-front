import { forwardRef, PropsWithChildren } from 'react';
import cn from 'clsx';
import './Button.scss';

interface ButtonProps
  extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>,
    React.AriaAttributes {
  variant?: 'button-1' | 'button-2' | 'button-3' | 'button-4' | 'button-5' | 'bare';
  isActive?: boolean;
  br?: '8' | '12' | '26';
}
const Button = forwardRef<HTMLButtonElement, PropsWithChildren<ButtonProps>>((props, ref) => {
  const { br, variant, children, className, isActive = false, disabled, ...rest } = props;

  const getClassName = (): string => {
    return cn({
      ['button-common']: true,
      ['active']: isActive,
      ['button-1']: variant === 'button-1',
      ['button-2']: variant === 'button-2',
      ['button-3']: variant === 'button-3',
      ['button-4']: variant === 'button-4',
      ['button-5']: variant === 'button-5',
      ['bare']: variant === 'bare',
      [`br-${br}`]: !!br,
      [`${className}`]: className,
      ['disabled']: !!disabled
    });
  };

  return (
    <button ref={ref} disabled={disabled} className={getClassName()} {...rest}>
      {children}
    </button>
  );
});

export default Button;
