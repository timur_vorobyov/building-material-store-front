import { ChangeEvent, forwardRef } from 'react';
import Typography from '../Typography/Typography';
import './InputText.scss';

interface InputTextProps
  extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  label?: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

const InputText = forwardRef<HTMLInputElement, InputTextProps>(
  ({ label, onChange, ...rest }, ref) => {
    return (
      <div className="input-text">
        {label ? (
          <Typography variant="label" fontWeight="bold">
            {label}
          </Typography>
        ) : null}
        <input
          ref={ref}
          type="text"
          {...rest}
          onChange={(event: ChangeEvent<HTMLInputElement>): void => {
            onChange(event);
          }}
        />
      </div>
    );
  }
);

export default InputText;
