import Check from './assets/image-components/Check';
import { FC } from 'react';
import './Checkbox.scss';
import cn from 'clsx';
import Typography from '../Typography/Typography';

interface CheckboxProps {
  isChecked: boolean;
  onChange: (checked: boolean) => void;
  isDisabled?: boolean;
  label?: string;
}

const Checkbox: FC<CheckboxProps> = ({ isChecked, isDisabled = false, onChange, label }) => {
  return (
    <div className="checkbox_container" onClick={() => !isDisabled && onChange(!isChecked)}>
      <div
        className={cn({
          checkbox: true,
          [`checkbox__icon-checked${isDisabled ? '-disabled' : ''}`]: isChecked,
          [`checkbox__icon${isDisabled ? '-disabled' : ''}`]: !isChecked
        })}>
        {isChecked ? <Check /> : null}
      </div>
      <div className="checkbox__label">
        <Typography variant="span">{label}</Typography>
      </div>
    </div>
  );
};

export default Checkbox;
