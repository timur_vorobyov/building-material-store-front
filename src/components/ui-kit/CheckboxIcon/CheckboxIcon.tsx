import { FC, ReactNode } from 'react';
import Button from '../Button/Button';
import Typography from '../Typography/Typography';
import './CheckboxIcon.scss';

interface CheckboxIconInterface {
  isChecked: boolean;
  onChange: (isChecked: boolean) => void;
  icon: ReactNode;
  label: string;
}

const CheckboxIcon: FC<CheckboxIconInterface> = ({ icon, label, isChecked, onChange }) => {
  return (
    <Button
      className="checkbox-icon"
      variant="button-3"
      isActive={isChecked}
      onClick={() => onChange(!isChecked)}>
      <div className="checkbox-icon__container">
        {icon}
        <Typography variant="span" fontWeight="medium" fontSize={14}>
          {label}
        </Typography>
      </div>
    </Button>
  );
};

export default CheckboxIcon;
