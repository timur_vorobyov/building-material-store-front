import { FC, PropsWithChildren } from 'react';
import cn from 'clsx';
import './Typography.scss';

export type TypographyProps = {
  variant: 'p' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'li' | 'span' | 'label';
  className?: string;
  style?: React.CSSProperties;
  color?: string;
  fontWeight?: 'bold' | 'regular' | 'medium' | 'semi-bold';
  lh?: string;
  fontSize?: 12 | 14 | 18 | 20 | 22 | 24 | 26 | 32 | 48 | 64;
};

const Typography: FC<PropsWithChildren<TypographyProps>> = (props) => {
  const { variant, children, style, color, fontSize, className } = props;
  const Component = variant;
  const compStyle = { ...style };

  const getClassName = (): string => {
    const { fontWeight, lh } = props;
    return cn({
      [`font-size-${fontSize}`]: !!fontSize,
      ['white']: color === 'white',
      ['red']: color === 'red',
      ['bold']: fontWeight === 'bold',
      ['semi-bold']: fontWeight === 'semi-bold',
      ['regular']: fontWeight === 'regular',
      ['medium']: fontWeight === 'medium',
      [`ln-${lh}`]: lh,
      [`${className}`]: className
    });
  };

  return (
    <Component style={compStyle} className={getClassName()}>
      {children}
    </Component>
  );
};

export default Typography;
