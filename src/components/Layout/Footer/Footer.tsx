import './Footer.scss';
import translate from 'i18n/translate';
import Typography from 'components/ui-kit/Typography/Typography';

const Footer = () => {
  return (
    <footer>
      <div className="footer__info">
        <Typography variant="span" color="white">
          <>© 2021 Vector. {translate('rightsProtected')}</>
        </Typography>
      </div>
    </footer>
  );
};

export default Footer;
