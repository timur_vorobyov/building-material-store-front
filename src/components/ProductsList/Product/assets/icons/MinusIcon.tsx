const MinusIcon = () => {
  return (
    <svg viewBox="0 0 64 64" role="img">
      <path
        data-name="layer1"
        fill="black"
        stroke="black"
        strokeMiterlimit="10"
        strokeWidth="4"
        d="M48 32H16"
        strokeLinejoin="round"
        strokeLinecap="round"></path>
    </svg>
  );
};

export default MinusIcon;
