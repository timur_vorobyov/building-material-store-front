const PlusIcon = () => {
  return (
    <svg viewBox="0 0 64 64" role="img">
      <path d="M53 29H35V11h-6v18H11v6h18v18h6V35h18v-6z" fill="black" data-name="layer1"></path>
    </svg>
  );
};

export default PlusIcon;
