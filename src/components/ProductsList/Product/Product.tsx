import { FC } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import './Product.scss';
import HeartIcon from './assets/icons/HeartIcon';
import { useAuth } from '@contexts/auth/use-auth-hook';
import { useAuthModal } from '@contexts/auth-modal/use-auth-modal-hook';
import { ProductInterface } from '@services/product/interface/product.interface';
import { useMutation } from 'react-query';
import cn from 'clsx';
import { useProductService } from '@services/product/use-product-service-hook';
import Typography from 'components/ui-kit/Typography/Typography';
import Button from 'components/ui-kit/Button/Button';
import { useLocale } from '@contexts/locale/use-locale-hook';
import { LocalesEnum } from 'i18n';
import CartController from 'components/CartController/CartController';
import { APP_MEDIA_URL } from '@config';
import { formatPrice } from '@helpers/text-fromatter.helper';
import { routes } from '@consts/route.consts';
import { replacePathParams } from '@helpers/route.helper';

interface ProductProps {
  product: ProductInterface;
}
const Product: FC<ProductProps> = ({ product }) => {
  const location = useLocation();
  const isFavoritePage = location.pathname === routes.favoriteProduct;
  const { locale } = useLocale();
  const names = {
    [LocalesEnum.Russian]: product.nameRu,
    [LocalesEnum.Uzbek]: product.nameUz,
    [LocalesEnum.English]: product.nameEn
  };
  const { imagesUrls, price, id } = product;
  const { setAuthModalData } = useAuthModal();
  const navigate = useNavigate();
  const { user, favorite, setFavorite } = useAuth();

  const service = useProductService();
  const { mutate: addToFavorite } = useMutation({
    mutationKey: 'addToFavorite',
    mutationFn: (params: { userId: number; productId: number }) => service.addToFavorite(params),
    onSuccess: (data) => {
      setFavorite([...favorite, data.data.id]);
    }
  });
  const { mutate: removeFromFavorite } = useMutation({
    mutationKey: 'removeFromFavorite',
    mutationFn: (params: { userId: number; productId: number }) =>
      service.removeFromFavorite(params),
    onSuccess: (data) => {
      setFavorite(favorite.filter((favoriteProductId) => favoriteProductId !== data.data.id));
    }
  });

  const handleLikeClick = () => {
    if (!user) {
      setAuthModalData({
        isShown: true,
        isSignUp: false
      });
    } else {
      const params = {
        userId: user.id,
        productId: product.id
      };

      if (favorite.includes(id)) {
        removeFromFavorite(params);
      } else {
        addToFavorite(params);
      }
    }
  };

  return (
    <>
      {!isFavoritePage ? (
        <div
          onClick={() =>
            navigate(
              replacePathParams(routes.product, {
                id
              })
            )
          }
          className="product">
          <Button
            onClick={(e) => {
              e.stopPropagation();
              handleLikeClick();
            }}
            className={cn('product__like-button', { ['active']: favorite.includes(id) })}>
            <HeartIcon />
          </Button>
          <img src={APP_MEDIA_URL + imagesUrls[0]} className="img" />
          <div className="product-desc">
            <div className="product-desc__texts">
              <Typography variant="span" fontWeight="semi-bold" fontSize={18}>
                {formatPrice(price)}UZS{' '}
              </Typography>
              <Typography variant="span" fontSize={14}>
                {names[locale]}
              </Typography>
            </div>
            <CartController product={product} />
          </div>
        </div>
      ) : (
        <div className="product">
          <img src={APP_MEDIA_URL + imagesUrls[0]} className="img" />
          <div className="product-desc">
            <div className="product-desc__texts">
              <Typography variant="span" fontWeight="semi-bold" fontSize={18}>
                {formatPrice(price)}UZS{' '}
              </Typography>
              <Typography variant="span" fontSize={14}>
                {names[locale]}
              </Typography>
            </div>
            <Button
              variant="button-4"
              className="product-desc__in-favorite-btn"
              onClick={() => removeFromFavorite({ userId: user!.id, productId: product.id })}>
              <Typography variant="span" fontWeight="semi-bold" fontSize={18}>
                Удалить с понравившихся
              </Typography>
            </Button>
          </div>
        </div>
      )}
    </>
  );
};

export default Product;
