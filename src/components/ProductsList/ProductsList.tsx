import { ProductInterface } from '@services/product/interface/product.interface';
import { FC } from 'react';
import Product from './Product/Product';
import ProductSkeleton from './ProductSkeleton/ProductSkeleton';
import './ProductsList.scss';

interface ProductsListProps {
  products: ProductInterface[];
  isFetching: boolean;
}
const ProductsList: FC<ProductsListProps> = ({ isFetching, products }) => {
  return (
    <div className="products-list">
      {products.map((product) => (
        <Product key={product.id} product={product} />
      ))}
      {isFetching && <ProductSkeleton />}
    </div>
  );
};

export default ProductsList;
