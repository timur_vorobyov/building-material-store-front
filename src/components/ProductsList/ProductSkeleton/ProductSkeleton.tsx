import Skeleton from 'react-loading-skeleton';
import { DEFAULT_TAKE } from '../../../pages/HomePage/consts';
import './ProductSkeleton.scss';

const ProductSkeleton = () => {
  return (
    <>
      {Array(DEFAULT_TAKE)
        .fill(0)
        .map((_, key) => (
          <div key={key} className="product-skeleton">
            <div className="product-skeleton__image">
              <Skeleton />
            </div>
            <div>
              <Skeleton count={3} />
            </div>
          </div>
        ))}
    </>
  );
};

export default ProductSkeleton;
