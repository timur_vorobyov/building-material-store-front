import { Navigate } from 'react-router-dom';
import { routes } from '@consts/route.consts';
import { useAuth } from '@contexts/auth/use-auth-hook';

function PrivateRoute({ children }: { children: JSX.Element }) {
  const { user } = useAuth();
  return user ? children : <Navigate to={`${routes.home}`} />;
}

export default PrivateRoute;
