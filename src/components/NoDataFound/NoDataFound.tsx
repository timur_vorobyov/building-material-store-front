import SearchEmpty from './assets/icons/SearchEmpty';
import { FC } from 'react';
import Typography from '../ui-kit/Typography/Typography';
import './NoDataFound.scss';

interface NoDataFoundProps {
  title: string;
  subtitle?: string;
}
const NoDataFound: FC<NoDataFoundProps> = ({ title, subtitle }) => {
  return (
    <main className="no-data-found">
      <SearchEmpty />
      <div className="no-data-found__info">
        <Typography variant="span" fontSize={48} fontWeight="semi-bold">
          {title}
        </Typography>
        {subtitle && (
          <Typography variant="span" fontSize={24} fontWeight="medium">
            {subtitle}
          </Typography>
        )}
      </div>
    </main>
  );
};

export default NoDataFound;
