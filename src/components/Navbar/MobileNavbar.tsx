import { useAuth } from '@contexts/auth/use-auth-hook';
import './Navbar.scss';
import { Link } from 'react-router-dom';
import { routes } from '@consts/route.consts';
import Logo from './assets/images/logo.png';
import Button from 'components/ui-kit/Button/Button';
import MenuIcon from './assets/icons/MenuIcon';
import CartLink from './CartLink/CartLink';
import AvatarIcon from './assets/icons/AvatarIcon';
import { FC, useState } from 'react';
import ExitIcon from './assets/icons/ExitIcon';
import { useAuthModal } from '@contexts/auth-modal/use-auth-modal-hook';
import Modal from 'components/ui-kit/Modal/Modal';
import { INITIAL_AUTH_MODAL_VALUE } from '@contexts/auth-modal/consts';
import AuthForm from './AuthForm/AuthForm';
import cn from 'clsx';
import Typography from 'components/ui-kit/Typography/Typography';
import LanguageIcon from './assets/icons/LanguageIcon';
import { useLocale } from '@contexts/locale/use-locale-hook';
import { LocalesEnum } from 'i18n';
import useOutsideClick from 'utils/hooks/use-outside-click-hook';

interface MobileNavbarProps {
  handleModalShown?: (isModalShown: boolean) => void;
}

const MobileNavbar: FC<MobileNavbarProps> = () => {
  const { user, logOut } = useAuth();
  const { authModalData, setAuthModalData } = useAuthModal();
  const [isMobileMenuOpened, setIsMobileMenuOpened] = useState(false);
  const [isMobileLanguageShown, setIsMobileLanguageShown] = useState(false);
  const { locale, setLocaleData } = useLocale();
  const ref = useOutsideClick<HTMLButtonElement>(() => setIsMobileLanguageShown(false));

  const pathnames = [
    {
      url: routes.order,
      title: 'Orders'
    }
  ];

  return (
    <nav className="mobile-navbar">
      <Modal
        width="max"
        title={authModalData.isSignUp ? 'Регистрация' : 'Авторизация'}
        isModalShown={!user && authModalData.isShown}
        handleHideModal={() => {
          setAuthModalData(INITIAL_AUTH_MODAL_VALUE);
        }}
        body={<AuthForm />}
      />
      <div className={'mobile-navbar__top'}>
        <Link to={routes.home}>
          <img src={Logo} alt="logo" />
        </Link>
        <Button onClick={() => setIsMobileMenuOpened(!isMobileMenuOpened)}>
          <MenuIcon />
        </Button>
      </div>

      <div className={cn({ ['active']: isMobileMenuOpened }, 'mobile-navbar__list')}>
        <ul className="navbar__list">
          {pathnames.map((pathname, index) => (
            <Typography key={index} variant="li">
              <Link to={pathname.url}>{pathname.title}</Link>
            </Typography>
          ))}
        </ul>
      </div>
      {isMobileLanguageShown && (
        <div className="language-switch__dropdown">
          <Button
            className={cn({ ['selected']: locale === LocalesEnum.Uzbek })}
            onClick={() => setLocaleData(LocalesEnum.Uzbek)}>
            <Typography variant="span">O'zbekcha</Typography>
          </Button>
          <Button
            className={cn({ ['selected']: locale === LocalesEnum.English })}
            onClick={() => setLocaleData(LocalesEnum.English)}>
            <Typography variant="span">English</Typography>
          </Button>
          <Button
            className={cn({ ['selected']: locale === LocalesEnum.Russian })}
            onClick={() => setLocaleData(LocalesEnum.Russian)}>
            <Typography variant="span">Русский</Typography>
          </Button>
        </div>
      )}

      <div className={'mobile-navbar__bottom'}>
        <CartLink />
        <Button
          ref={ref}
          onClick={() => setIsMobileLanguageShown(!isMobileLanguageShown)}
          variant="bare">
          <LanguageIcon />
        </Button>
        {user ? (
          <Button onClick={() => logOut()}>{<ExitIcon />}</Button>
        ) : (
          <Button
            className={'authIcon'}
            onClick={() => setAuthModalData({ isShown: true, isSignUp: false })}>
            <AvatarIcon />
          </Button>
        )}
      </div>
    </nav>
  );
};

export default MobileNavbar;
