import './LanguageSwitch.scss';
import LanguageIcon from '../assets/icons/LanguageIcon';
import { useOutsideClick } from 'utils/hooks/use-outside-click-hook';
import { FC, useState } from 'react';
import cn from 'clsx';
import Typography from 'components/ui-kit/Typography/Typography';
import Button from 'components/ui-kit/Button/Button';
import { useLocale } from '@contexts/locale/use-locale-hook';
import { LocalesEnum } from 'i18n';

const LanguageSwitch: FC = () => {
  const [isShown, setIsShown] = useState(false);
  const ref = useOutsideClick<HTMLButtonElement>(() => setIsShown(false));
  const { locale, setLocaleData } = useLocale();

  return (
    <div className="language-switch">
      <Button ref={ref} onClick={() => setIsShown(!isShown)} variant="bare">
        <LanguageIcon />
      </Button>
      {isShown ? (
        <div className="language-switch__dropdown">
          <Button
            className={cn({ ['selected']: locale === LocalesEnum.Uzbek })}
            onClick={() => setLocaleData(LocalesEnum.Uzbek)}>
            <Typography variant="span">O'zbekcha</Typography>
          </Button>
          <Button
            className={cn({ ['selected']: locale === LocalesEnum.English })}
            onClick={() => setLocaleData(LocalesEnum.English)}>
            <Typography variant="span">English</Typography>
          </Button>
          <Button
            className={cn({ ['selected']: locale === LocalesEnum.Russian })}
            onClick={() => setLocaleData(LocalesEnum.Russian)}>
            <Typography variant="span">Русский</Typography>
          </Button>
        </div>
      ) : null}
    </div>
  );
};

export default LanguageSwitch;
