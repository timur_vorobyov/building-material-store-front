import './UserMenu.scss';
import MenuIcon from '../assets/icons/MenuIcon';
import AvatarIcon from '../assets/icons/AvatarIcon';
import { FC, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { useOutsideClick } from 'utils/hooks/use-outside-click-hook';
import AuthContext from '@contexts/auth/auth.context';
import { useAuthModal } from '@contexts/auth-modal/use-auth-modal-hook';
import { routes } from '@consts/route.consts';
import Typography from 'components/ui-kit/Typography/Typography';
import Button from 'components/ui-kit/Button/Button';
import translate from 'i18n/translate';

const UserMenu: FC = () => {
  const [isShown, setIsShown] = useState(false);
  const ref = useOutsideClick<HTMLButtonElement>(() => setIsShown(false));
  const { user, logOut } = useContext(AuthContext);
  const { setAuthModalData } = useAuthModal();
  return (
    <div className="user-menu">
      <Button ref={ref} variant="button-1" onClick={() => setIsShown(!isShown)}>
        <div className="user-menu__btn-content">
          <MenuIcon />
          <AvatarIcon />
        </div>
      </Button>
      {isShown ? (
        <div className="user-menu__dropdown">
          <div className="user-menu__dropdown-top user-menu__dropdown-item">
            {user ? (
              <>
                <Link to={routes.favoriteProduct}>
                  <Typography variant="span">Понравившиеся</Typography>
                </Link>
                <Link to={routes.myProductsList}>
                  <Typography variant="span">Ваши товары</Typography>
                </Link>
                <Link to={routes.userOrder}>
                  <Typography variant="span">Ваши заказы</Typography>
                </Link>
              </>
            ) : (
              <>
                <Button onClick={() => setAuthModalData({ isShown: true, isSignUp: true })}>
                  <Typography variant="span">Зарегистрироваться</Typography>
                </Button>
                <Button onClick={() => setAuthModalData({ isShown: true, isSignUp: false })}>
                  <Typography variant="span">Войти</Typography>
                </Button>
              </>
            )}
          </div>
          <div className="user-menu__dropdown-bottom user-menu__dropdown-item">
            <Link to={'/'}>
              <Typography variant="span">Помощь</Typography>
            </Link>
            {user ? (
              <Button
                onClick={() => {
                  setAuthModalData({ isShown: false, isSignUp: false });
                  logOut();
                }}>
                <Typography variant="span">{translate('logOut')}</Typography>
              </Button>
            ) : null}
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default UserMenu;
