import { FC } from 'react';
import DesctopNavbar from './DesctopNavbar';
import MobileNavbar from './MobileNavbar';
import './Navbar.scss';

interface NavbarProps {
  handleModalShown?: (isModalShown: boolean) => void;
}

const Navbar: FC<NavbarProps> = ({ handleModalShown }) => {
  return (
    <>
      <MobileNavbar handleModalShown={handleModalShown} />
      <DesctopNavbar handleModalShown={handleModalShown} />
    </>
  );
};

export default Navbar;
