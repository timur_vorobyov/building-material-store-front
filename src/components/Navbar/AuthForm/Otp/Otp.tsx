import LoginDto from '@contexts/auth/dto/login.dto';
import SignUpDto from '@contexts/auth/dto/sign-up.dto';
import { useAuth } from '@contexts/auth/use-auth-hook';
import { useAuthService } from '@services/auth/use-auth-service-hook';
import { useUserService } from '@services/user/use-user-service-hook';
import FormHelperText from 'components/FormHelperText/FormHelperText';
import InputText from 'components/ui-kit/InputText/InputText';
import jwt_decode from 'jwt-decode';
import { FC, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import Layout from '../Layout/Layout';

interface OtpProps {
  isSignUp: boolean;
  data: Omit<SignUpDto, 'password'> | Omit<LoginDto, 'password'>;
}
const Otp: FC<OtpProps> = ({ isSignUp, data }) => {
  const authService = useAuthService();
  const usersService = useUserService();
  const { updateAuthData } = useAuth();
  const [serverSideError, setServerSideError] = useState('');
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm<SignUpDto | LoginDto>({
    defaultValues: {
      ...data,
      password: ''
    }
  });

  const onSubmit = async (dataWithOtpCode: SignUpDto | LoginDto) => {
    try {
      const { accessToken } = isSignUp
        ? await authService.signUp(dataWithOtpCode as SignUpDto)
        : await authService.login(dataWithOtpCode as LoginDto);

      if (typeof accessToken === 'undefined') {
        setServerSideError('Wrong varification code');
        return;
      }
      const decoded: { id: number } = jwt_decode(accessToken);

      if (!decoded || !decoded.id) {
        setServerSideError('Wrong token');
        return;
      }

      const profile = await usersService.single(decoded.id);
      updateAuthData(profile.data);
    } catch (e) {
      console.log({ message: (e as Error).message, state: 'error' });
    }
  };
  return (
    <Layout btnContent="Отправить код" onSubmit={handleSubmit(onSubmit)}>
      <Controller
        defaultValue=""
        control={control}
        name="password"
        rules={{
          required: 'Field is required'
        }}
        render={({ field }) => <InputText placeholder="Введите код" label="Код" {...field} />}
      />
      {!serverSideError && errors.password && (
        <FormHelperText>{errors.password.message as string}</FormHelperText>
      )}
      {serverSideError && <FormHelperText>{serverSideError}</FormHelperText>}
    </Layout>
  );
};

export default Otp;
