import SignUpDto from '@contexts/auth/dto/sign-up.dto';
import { useAuthService } from '@services/auth/use-auth-service-hook';
import FormHelperText from 'components/FormHelperText/FormHelperText';
import InputPhone from 'components/ui-kit/InputPhone/InputPhone';
import InputText from 'components/ui-kit/InputText/InputText';
import { FC, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import Layout from '../Layout/Layout';
import Otp from '../Otp/Otp';
import './SignUp.scss';

type Data = Omit<SignUpDto, 'otpCode'>;
const SignUp: FC = () => {
  const [dataState, setDataState] = useState<Data | undefined>();
  const [serverSideError, setServerSideError] = useState('');
  const authService = useAuthService();

  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm<Data>({
    defaultValues: {
      name: '',
      phone: ''
    }
  });

  const handleGetOtpCode = async (data: Data) => {
    const response = await authService.sendOtpSignUpCode(data.phone);
    if (response.data === 'PHONE_NUMBER_ALREADY_EXIST') {
      setServerSideError('Phone number already exist');
      return;
    }
    setDataState(data);
  };

  if (dataState) return <Otp data={dataState} isSignUp={true} />;

  return (
    <Layout btnContent="Получить код" onSubmit={handleSubmit(handleGetOtpCode)}>
      <div className="sign-up__name">
        <Controller
          defaultValue=""
          control={control}
          name="name"
          rules={{
            required: 'Field is required'
          }}
          render={({ field }) => <InputText label="Имя" placeholder="Введите имя" {...field} />}
        />
        {!serverSideError && errors.name && (
          <FormHelperText>{errors.name.message as string}</FormHelperText>
        )}
      </div>
      <Controller
        defaultValue=""
        control={control}
        name="phone"
        rules={{
          required: 'Field is required'
        }}
        render={({ field }) => <InputPhone {...field} />}
      />
      {!serverSideError && errors.phone && (
        <FormHelperText>{errors.phone.message as string}</FormHelperText>
      )}
      {serverSideError && <FormHelperText>{serverSideError}</FormHelperText>}
    </Layout>
  );
};

export default SignUp;
