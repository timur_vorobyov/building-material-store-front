import Login from './Login/Login';
import SignUp from './SignUp/SignUp';
import { FC } from 'react';
import './AuthForm.scss';
import { useAuthModal } from '@contexts/auth-modal/use-auth-modal-hook';
import Button from 'components/ui-kit/Button/Button';
import Typography from 'components/ui-kit/Typography/Typography';
import './AuthForm.scss';
const AuthForm: FC = () => {
  const { authModalData, setAuthModalData } = useAuthModal();

  return (
    <div className="auth-form">
      {authModalData.isSignUp ? <SignUp /> : <Login />}
      <div className="auth-form__footer">
        {authModalData.isSignUp ? (
          <Button onClick={() => setAuthModalData({ ...authModalData, isSignUp: false })}>
            <Typography variant="span" fontWeight="bold">
              Логин
            </Typography>
          </Button>
        ) : (
          <Button onClick={() => setAuthModalData({ ...authModalData, isSignUp: true })}>
            <Typography variant="span" fontWeight="bold">
              Регистрация
            </Typography>
          </Button>
        )}
      </div>
    </div>
  );
};

export default AuthForm;
