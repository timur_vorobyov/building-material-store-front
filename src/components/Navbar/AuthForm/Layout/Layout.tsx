import { FC, PropsWithChildren } from 'react';
import './Layout.scss';
import Typography from 'components/ui-kit/Typography/Typography';
import Button from 'components/ui-kit/Button/Button';

interface LatoutProps {
  btnContent: string;
  onSubmit: () => void;
}

const Layout: FC<PropsWithChildren<LatoutProps>> = ({ btnContent, onSubmit, children }) => {
  return (
    <div className="auth-form-layout">
      {children}
      <Button variant="button-4" onClick={() => onSubmit()}>
        <Typography variant="span" fontSize={20} fontWeight="bold">
          {btnContent}
        </Typography>
      </Button>
    </div>
  );
};

export default Layout;
