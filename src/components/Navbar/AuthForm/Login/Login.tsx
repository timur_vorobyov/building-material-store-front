import LoginDto from '@contexts/auth/dto/login.dto';
import { useAuthService } from '@services/auth/use-auth-service-hook';
import FormHelperText from 'components/FormHelperText/FormHelperText';
import InputPhone from 'components/ui-kit/InputPhone/InputPhone';
import { FC, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import Layout from '../Layout/Layout';
import Otp from '../Otp/Otp';

type Data = Omit<LoginDto, 'otpCode'>;

const Login: FC = () => {
  const [dataState, setDataState] = useState<Data | undefined>();
  const authService = useAuthService();
  const [serverSideError, setServerSideError] = useState('');

  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm<Data>({
    defaultValues: {
      phone: ''
    }
  });

  const handleGetOtpCode = async (data: Data) => {
    const response = await authService.sendOtpLoginCode(data);
    if (response.data === 'PHONE_NUMBER_DOES_NOT_EXIST') {
      setServerSideError("Phone number doesn't exist");
      return;
    }
    setDataState(data);
  };

  if (dataState) {
    return <Otp data={dataState} isSignUp={false} />;
  }

  return (
    <Layout btnContent="Получить код" onSubmit={handleSubmit(handleGetOtpCode)}>
      <Controller
        defaultValue=""
        control={control}
        name="phone"
        rules={{
          required: 'Field is required'
        }}
        render={({ field }) => <InputPhone {...field} />}
      />
      {!serverSideError && errors.phone && (
        <FormHelperText>{errors.phone.message as string}</FormHelperText>
      )}
      {serverSideError && <FormHelperText>{serverSideError}</FormHelperText>}
    </Layout>
  );
};

export default Login;
