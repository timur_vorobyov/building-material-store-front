import { routes } from '@consts/route.consts';
import { useAuth } from '@contexts/auth/use-auth-hook';
import Typography from 'components/ui-kit/Typography/Typography';
import { Link } from 'react-router-dom';
import CartIcon from '../assets/icons/CartIcon';
import './CartLink.scss';

const CartLink = () => {
  const { cart } = useAuth();
  const productCartAmount = cart.reduce((total, c) => {
    return total + c.count;
  }, 0);

  return (
    <Link className="cart-icon-link" to={routes.order}>
      <div className={'cart-icon-amount'}>
        <Typography variant="span" fontSize={12} color="white">
          {productCartAmount}
        </Typography>
      </div>
      <CartIcon />
    </Link>
  );
};

export default CartLink;
