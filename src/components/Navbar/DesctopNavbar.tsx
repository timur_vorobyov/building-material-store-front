import { Link } from 'react-router-dom';
import { routes } from '@consts/route.consts';
import { FC } from 'react';
import UserMenu from './UserMenu/UserMenu';
import cn from 'clsx';
import AuthForm from './AuthForm/AuthForm';
import { useAuth } from '@contexts/auth/use-auth-hook';
import { useAuthModal } from '@contexts/auth-modal/use-auth-modal-hook';
import { INITIAL_AUTH_MODAL_VALUE } from '@contexts/auth-modal/consts';
import Typography from 'components/ui-kit/Typography/Typography';
import SearchIcon from './assets/icons/SearchIcon';
import Logo from './assets/images/logo.png';
import Button from 'components/ui-kit/Button/Button';
import Modal from 'components/ui-kit/Modal/Modal';
import CartLink from './CartLink/CartLink';

interface DesctopNavbarProps {
  handleModalShown?: (isModalShown: boolean) => void;
}

const DesctopNavbar: FC<DesctopNavbarProps> = ({ handleModalShown }) => {
  const { user } = useAuth();
  const { authModalData, setAuthModalData } = useAuthModal();

  return (
    <nav className="desctop-navbar">
      <Modal
        width="min"
        title={authModalData.isSignUp ? 'Регистрация' : 'Авторизация'}
        isModalShown={!user && authModalData.isShown}
        handleHideModal={() => {
          setAuthModalData(INITIAL_AUTH_MODAL_VALUE);
        }}
        body={<AuthForm />}
      />
      <div
        className={cn('desctop-navbar__container', {
          grid: handleModalShown,
          flex: !handleModalShown
        })}>
        <div className="desctop-navbar__left">
          <Link to={routes.home}>
            <img src={Logo} alt="logo" />
          </Link>
        </div>
        {handleModalShown && (
          <div className="desctop-navbar__middle">
            <Button variant="button-1" onClick={() => handleModalShown(true)}>
              <div className="button-container">
                <Typography variant="span" fontWeight="semi-bold">
                  Фильтры
                </Typography>
                <div className="search-icon">
                  <SearchIcon />
                </div>
              </div>
            </Button>
          </div>
        )}
        <div className="desctop-navbar__right">
          {/* <LanguageSwitch /> */}
          <CartLink />
          <UserMenu />
        </div>
      </div>
    </nav>
  );
};

export default DesctopNavbar;
