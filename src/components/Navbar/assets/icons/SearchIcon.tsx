const SearchIcon = () => {
  return (
    <svg width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M8.375 7.875L11.75 11.25M5.375 9C7.65317 9 9.5 7.15317 9.5 4.875C9.5 2.59683 7.65317 0.75 5.375 0.75C3.09683 0.75 1.25 2.59683 1.25 4.875C1.25 7.15317 3.09683 9 5.375 9Z"
        stroke="white"
        strokeWidth="1.6"
      />
    </svg>
  );
};

export default SearchIcon;
