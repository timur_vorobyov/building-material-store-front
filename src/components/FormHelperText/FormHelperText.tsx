import Typography from 'components/ui-kit/Typography/Typography';
import { FC, PropsWithChildren } from 'react';

const FormHelperText: FC<PropsWithChildren> = ({ children }) => {
  return (
    <Typography variant="span" color="red">
      {children}
    </Typography>
  );
};

export default FormHelperText;
