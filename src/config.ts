export const NODE_ENV = process.env.NODE_ENV || 'development';
export const API_URL = process.env.REACT_APP_API_URL || 'http://localhost:9000';
export const COOKIE_SECRET = process.env.REACT_APP_COOKIE_SECRET || '123456';
export const APP_MEDIA_URL = process.env.REACT_APP_MEDIA_URL || 'http://localhost:9000';
export const YANDEX_API_KEY = process.env.YANDEX_API_KEY || 'e4f97ce3-8e6e-4ca6-82bb-ed69f35c7463';
