import { useLocale } from '@contexts/locale/use-locale-hook';
import { IntlProvider } from 'react-intl';
import messages from './messages';

type Provider = {
  children: JSX.Element;
};

const IntlWrapperContextProvider = ({ children }: Provider) => {
  const { locale } = useLocale();

  return (
    <IntlProvider locale={locale} messages={messages[locale]}>
      {children}
    </IntlProvider>
  );
};

export default IntlWrapperContextProvider;
