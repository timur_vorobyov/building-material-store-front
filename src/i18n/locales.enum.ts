export enum LocalesEnum {
  English = 'en-us',
  Uzbek = 'uz-uz',
  Russian = 'ru'
}
