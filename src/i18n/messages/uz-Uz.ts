import { LocalesEnum } from 'i18n/locales.enum';

export default {
  [LocalesEnum.Uzbek]: {
    lookAtOrder: "Buyurtmani ko'rish",
    landingPhrase: 'SIZ QURVOT-\nGANIZDA BIZA YETKAZAMIZA.',
    companyPurpose: 'VEKTOR qurilish materiallarini bozorlardan etkazib beriladi.',
    call: 'BUNI QILAYLIK',
    order: 'BUYURTMA BERISH',
    deliver: 'YETKAZIB BERISH',
    build: 'QURISH',
    orderDesc:
      'Qurilish mollarini to’g’ridan to’g’ri bizning saytdan sotib oling yoki bizdan istalgan etkazib beruvchilardan qurilish mollarini olib ketishimizni so’rang.',
    deliverDesc:
      'Biz kerakli qurilish mollarini to’playmiz va to’g’ri ish joyingizga etkazib beramiz.',
    buildDesc:
      'Siz o’z loyixangiz ustida ishlayotgan payt biz qurilish mollarini sizga kerakli joyga enkazib beramiz.',
    filters: 'Filtrlar',
    inCart: 'Savatchaga',
    categories: 'Mahsulot nomlari',
    markets: 'Bozorlar',
    emptyFavorite: "Savatingiz bo'sh",
    signIn: 'Kirish',
    logOut: 'Chiqish',
    login: 'Kirish',
    registration: "Ro'yxatdan o'tish",
    phoneNumber: 'Raqam',
    name: 'Ism',
    search: 'Qidirish',
    contacts: 'Aloqa',
    yourFavorite: 'Savatingiz',
    placingAnOrder: 'Buyurtma',
    placeAnOrder: 'Buyurtma qilish',
    product: 'Mahsulot',
    amount: 'Soni',
    sum: 'Narxi',
    total: 'Jami',
    callQuestions: "Savollaringiz bormi? Bizga qo'ng'iroq qiling!",
    pointAddress: 'Manzilni kiritish',
    changeAddress: "Manzilni o'zgartirish",
    confirmAddress: 'Manzilni tasdiqlash',
    uzbekSum: ' sum',
    errorAddress: 'Iltimos manzilni kiriting',
    aboutCompany: 'Kompaniya haqida',
    aboutCompanyText:
      'VECTOR kompaniyasi mijozlarga Toshkent shahridagi istalgan qurilish mollari bozoridan o’rningizdan turmay buyurtma qilish imkonini beradi. Biz mijoz va eltib beruvchilarni birlashtiramiz.',
    paymentConditions: "To'lov shartlari",
    paymentConditionsText:
      ' To’lov turi naqd, siz etkazib berilgan maxsulotni sofligini kursatilgan talabga asosan ko’zdan kechirishingiz mumkin. Qabul qiluvchi yo’l varaqasini imzolaydi, pulni to’laydi va buyurtmani qabul qilib oladi.',
    deliveryConditions: 'Yetkazish shartlari',
    deliveryConditionsText1:
      '- Sizning buyurtmangiz qabul qilingandan so’ng, sizga xabarchimiz aloqaga chiqib, etkazib berishning qulay vaqtini aniqlab oladi.',
    deliveryConditionsText2:
      '- Etkazib berish xizmatining ish vaqti 9:00 dan 19:00 gacha. Dam olish kunlari yo’q.',
    deliveryConditionsText3:
      '- Sizdan iltimos maxsulotnin eltib beruvchining oldida ochib, maxsulotni ko’zdan kechiring va ruyxat bo’yicha tekshirib oling.',
    deliveryConditionsText4:
      '- Qabul qilish vaqti cheklangan (15 daqiqa). Qabul qilish vaqti tugaganidan so’ng, siz buyurtmani bekor qila olmaysiz.',
    productWarranty: 'Mahsulot kafolati',
    productWarrantyText1:
      'Kafolat davri - bu mijoz tarafidan maxsulotning sifati buzilganligi yoki almashtirish choralarini talab qilishi mumkin bo’lgan vaqt. ',
    productWarrantyText2:
      'Kafolat mijoz maxsulotni qabul qilib olib, etkazib berishning pulini to’lagan vaqtgacha amal qiladi.',
    orderProcessed: 'Buyurtma qayta ishlandi',
    backToHome: 'Asosiy sahifaga qaytish',
    rightsProtected: 'Barcha huquqlar himoyalangan.',
    receiveCode: 'Kodni qabul qilish',
    userDataTitle: "Foydalanuvchi ma'lumotlari",
    unRegisteredError: "Bu raqam ro'yxatdan o'tmagan",
    unLoggedError: "Bu raqam allaqachon ro'yxatdan o'tgan"
  }
};
