import { LocalesEnum } from 'i18n/locales.enum';

export default {
  [LocalesEnum.English]: {
    lookAtOrder: 'Shopping favorite',
    landingPhrase: 'YOU BUILD \nWHILE WE DELIVER.',
    companyPurpose: 'VECTOR delivers construction materials from bazars.',
    call: "LET'S DO THIS",
    order: 'ORDER',
    deliver: 'DELIEVER',
    build: 'BUILD',
    orderDesc: 'Purchase materials directly within our site or have us pick up from any supplier.',
    deliverDesc: "We'll pick the materials up and bring them directly to your job site.",
    buildDesc: 'You work on the project while we bring the materials you need, when you need them.',
    filters: 'Filters',
    inCart: 'In cart',
    categories: 'Categories',
    markets: 'Markets',
    emptyFavorite: 'Your favorite is empty',
    signIn: 'Log in',
    logOut: 'Log out',
    login: 'Authorization',
    registration: 'Sign up',
    phoneNumber: 'Phone number',
    name: 'Name',
    search: 'Search',
    contacts: 'Contacts',
    yourFavorite: 'Your favorite',
    placingAnOrder: 'Place an order',
    placeAnOrder: 'Place an order',
    product: 'Product',
    amount: 'Amount',
    sum: 'Sum',
    total: 'Total',
    callQuestions: 'Have questions? Just call us!',
    pointAddress: 'Point the address',
    changeAddress: 'Change the address',
    confirmAddress: 'Confirm the address',
    uzbekSum: ' sum',
    errorAddress: 'Please point the address',
    aboutCompany: 'About company',
    aboutCompanyText:
      'The VECTOR company allows users to order building materials from any markets in Tashkent, sitting on the couch. We connect customers with the suppliers they need to finish their projects.',
    paymentConditions: 'Payment conditions',
    paymentConditionsText:
      'Payment is made in cash, you can inspect the delivered goods for damage, compliance with the specified conditions. A customer signs the shipping documents, pays and receives the goods.',
    deliveryConditions: 'Delivery conditions',
    deliveryConditionsText1:
      '- Having received your order, our manager will connect with you to clarify a convenient delivery time and a delivery price.',
    deliveryConditionsText2: '- Delivery working hours from 9.00 to 19.00 seven days a week.',
    deliveryConditionsText3:
      '- Earnest request! Open the goods in front of the courier, inspect for the integrity of the goods and compliance with the specified configuration.',
    deliveryConditionsText4:
      '- Inspection and fitting time is limited (15 min). After this time you can not refuse partially or completely from the purchase.',
    productWarranty: 'Product warranty',
    productWarrantyText1:
      'The product warranty is the period during which the customer, having discovered a defect in the product, has the right to demand that actions to be taken to eliminate the defect or replace the product.',
    productWarrantyText2:
      'The guarantee lasts until the customer accepts the goods and pays for this delivery.',
    orderProcessed: 'Order processed',
    backToHome: 'Back to homepage',
    rightsProtected: 'Your rights are protected',
    receiveCode: 'Get the code',
    userDataTitle: 'User data',
    unRegisteredError: 'This number is not registered',
    unLoggedError: 'This number is already registered'
  }
};
