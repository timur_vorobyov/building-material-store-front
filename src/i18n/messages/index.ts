import en from './en-US';
import ru from './ru';
import uz from './uz-Uz';

export default {
  ...en,
  ...ru,
  ...uz
};
