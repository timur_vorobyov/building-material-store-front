import { BrowserRouter } from 'react-router-dom';
import Router from './routes';
import '@styles/global.scss';
import IntlWrapperContextProvider from 'i18n/intl-wrapper.provider';
import LocaleContextProvider from '@contexts/locale/locale.provider';
import AuthContextProvider from '@contexts/auth/auth.provider';
import GlobalServices from '@services/global-services';
import AuthModalContextProvider from '@contexts/auth-modal/auth-modal.provider';
import { QueryClient, QueryClientProvider } from 'react-query';

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false
    }
  }
});

function App() {
  return (
    <GlobalServices>
      <AuthContextProvider>
        <QueryClientProvider client={queryClient}>
          <LocaleContextProvider>
            <IntlWrapperContextProvider>
              <AuthModalContextProvider>
                <BrowserRouter>
                  <Router />
                </BrowserRouter>
              </AuthModalContextProvider>
            </IntlWrapperContextProvider>
          </LocaleContextProvider>
        </QueryClientProvider>
      </AuthContextProvider>
    </GlobalServices>
  );
}

export default App;
