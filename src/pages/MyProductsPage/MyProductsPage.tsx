import { useProductService } from '@services/product/use-product-service-hook';
import './MyProductsPage.scss';
import { useQuery } from 'react-query';
import { useAuth } from '@contexts/auth/use-auth-hook';
import ProductsList from 'components/ProductsList/ProductsList';
import Heading from 'components/Heading/Heading';
import Container from 'components/Container/Container';
import Navbar from 'components/Navbar/Navbar';

const MyProductsPage = () => {
  const { user } = useAuth();
  const service = useProductService();
  const { data, isFetching } = useQuery({
    queryKey: ['users-products'],
    queryFn: () => service.myProductsList({ userId: user!.id }),
    initialData: []
  });

  if (isFetching || !data) return <></>;

  return (
    <>
      <Navbar />
      <main className="my-products__container">
        <Container>
          <Heading title={'Мои товары'} />
        </Container>
        <ProductsList isFetching={isFetching} products={data} />
      </main>
    </>
  );
};

export default MyProductsPage;
