import { formatPrice } from '@helpers/text-fromatter.helper';
import Button from 'components/ui-kit/Button/Button';
import Typography from 'components/ui-kit/Typography/Typography';
import translate from 'i18n/translate';
import { StageNumbersEnum } from 'pages/OrderPage/enums';
import { FC } from 'react';
import Tabs from '../../../components/Tabs/Tabs';
import './DesctopOrderPage.scss';

interface DesctopOrderPageProps {
  currentStage: StageNumbersEnum;
  setCurrentStage: (currentStage: StageNumbersEnum) => void;
  stages: {
    [key: number]: JSX.Element;
  };
  totalPrice: number;
}

const DesctopOrderPage: FC<DesctopOrderPageProps> = ({
  stages,
  setCurrentStage,
  currentStage,
  totalPrice
}) => {
  return (
    <div className={'desctop-order'}>
      <div className={'desctop-order-left'}>
        <Tabs
          currentStage={currentStage}
          setCurrentStage={setCurrentStage}
          firstTitle={translate('yourFavorite')}
          secondTitle={translate('placeAnOrder')}
        />
        {stages[currentStage]}
      </div>
      <div className={'desctop-order-right'}>
        <Typography variant="span" fontSize={24} fontWeight="semi-bold">
          {translate('total')}: {formatPrice(totalPrice)} UZS
        </Typography>
        <Button
          variant="button-4"
          className="prepare-order-btn"
          onClick={() =>
            currentStage === StageNumbersEnum.Cart
              ? setCurrentStage(StageNumbersEnum.Order)
              : console.log('form')
          }>
          <Typography variant="span"> {translate('placeAnOrder')}</Typography>
        </Button>
        <div className="general-info">
          <Typography variant="span" fontSize={18}>
            Есть вопросы! Звоните!
          </Typography>
          <Typography variant="span">+998 (90) 972-44-94</Typography>
        </div>
      </div>
    </div>
  );
};

export default DesctopOrderPage;
