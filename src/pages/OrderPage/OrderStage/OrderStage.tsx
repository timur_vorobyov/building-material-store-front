import Button from 'components/ui-kit/Button/Button';
import InputPhone from 'components/ui-kit/InputPhone/InputPhone';
import InputText from 'components/ui-kit/InputText/InputText';
import Modal from 'components/ui-kit/Modal/Modal';
import Typography from 'components/ui-kit/Typography/Typography';
import { useState } from 'react';
import './OrderStage.scss';
import { ProductDataAllowedValues, ProductDataInterface } from './types';
import Geolocation from './Geolocation/Geolocation';
import Geoloc from './assets/icons/Geoloc';

const OrderStage = () => {
  const [formData, setFormData] = useState({
    userName: '',
    phone: '',
    location: '',
    coordinates: [41.311194, 69.279774]
  });
  const [isModalShown, setIsModalShown] = useState(false);
  const changeOrderData = (name: keyof ProductDataInterface, value: ProductDataAllowedValues) => {
    setFormData((prev: ProductDataInterface): ProductDataInterface => {
      return {
        ...prev,
        [name]: value
      };
    });
  };

  return (
    <div className="order-stage">
      <Modal
        isGeo={true}
        title={''}
        isModalShown={isModalShown}
        handleHideModal={() => setIsModalShown(false)}
        body={
          <Geolocation
            changeOrderData={changeOrderData}
            coordinates={formData.coordinates}
            location={formData.location}
          />
        }
      />
      <div className="order-stage__containers">
        <div className="title">
          <Typography variant="span" fontWeight="semi-bold">
            Данные пользователя
          </Typography>
        </div>
        <form>
          <InputText onChange={() => console.log('sdjfn')} label="Имя" />
          <InputPhone onChange={() => console.log('sdjfn')} />
        </form>
      </div>

      <div className="order-stage__containers address">
        <div className="title">
          <Typography variant="span" fontWeight="semi-bold">
            Указать адрес
          </Typography>
        </div>
        <div className="bottom">
          <Geoloc />
          {formData.location && (
            <Typography variant="span" fontSize={26} fontWeight="bold" className="location">
              {formData.location}
            </Typography>
          )}

          <Button
            variant="button-4"
            className="prepare-order-btn"
            onClick={() => setIsModalShown(true)}>
            <Typography variant="span"> Указать адрес</Typography>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default OrderStage;
