export interface ProductDataInterface {
  userName: string;
  coordinates: number[];
  location: string;
  phone: string;
}

export type ProductDataAllowedValues = string | number | number[] | ProductDataInterface;

export type ChangeProductDataType = (
  name: keyof ProductDataInterface,
  value: ProductDataAllowedValues
) => void;
