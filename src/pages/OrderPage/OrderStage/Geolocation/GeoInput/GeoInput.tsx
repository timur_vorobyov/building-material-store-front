import { ChangeEvent, forwardRef } from 'react';
import GeoMark from './assets/icons/GeoMark';
import './GeoInput.scss';

interface GeoInputProps
  extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  label?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
}

const GeoInput = forwardRef<HTMLInputElement, GeoInputProps>(({ ...rest }, ref) => {
  return (
    <div className="geo-input">
      <GeoMark />
      <input className="input" id="suggest" ref={ref} type="text" placeholder="Адресс" {...rest} />
    </div>
  );
});

export default GeoInput;
