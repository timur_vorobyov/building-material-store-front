import { Map, ZoomControl, Placemark } from '@pbe/react-yandex-maps';
import { FC, memo } from 'react';
import axios from 'axios';
import './Location.scss';
import { YANDEX_API_KEY } from '@config';
import { AnyObject } from '@pbe/react-yandex-maps/typings/util/typing';
import { ChangeProductDataType, ProductDataInterface } from '../../types';

interface LocationProps {
  changeOrderData: ChangeProductDataType;
  coordinates: ProductDataInterface['coordinates'];
}

const Location: FC<LocationProps> = memo(({ changeOrderData, coordinates }) => {
  const changeCoordinates = (value: number[]) => {
    changeOrderData('coordinates', value);
  };

  const changeLocation = (value: string) => {
    changeOrderData('location', value);
  };

  const loadSuggest = (ymaps: AnyObject) => {
    const searchControl = new ymaps.control.SearchControl({
      options: {
        provider: 'yandex#map'
      }
    });

    const suggestView = new ymaps.SuggestView('suggest');
    suggestView.events.add('select', (e: AnyObject) => {
      searchControl.search(e.get('item').value).then((data: AnyObject) => {
        changeCoordinates(data.geoObjects.get(0).geometry.getCoordinates());
        changeLocation(data.metaData.geocoder.request);
      });
    });
  };

  const handleMapClick = async (e: AnyObject) => {
    const coords = e.get('coords');
    changeCoordinates(coords);
    const yanMapUrl = `https:/geocode-maps.yandex.ru/1.x?format=json&apikey=${YANDEX_API_KEY}&geocode=${coords[1]},${coords[0]}&results=1`;
    const response = await axios.get(yanMapUrl);
    const adressComponents =
      response.data.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty
        .GeocoderMetaData.Address.Components;
    let address = 'г.';
    adressComponents.forEach((element: any) => {
      if (element.kind == 'locality') {
        address += ' ' + element.name;
      }

      if (
        element.kind == 'district' ||
        element.kind == 'street' ||
        element.kind == 'vegetation' ||
        element.kind == 'house' ||
        element.kind == 'other'
      ) {
        address += ', ' + element.name;
      }
    });
    changeLocation(address);
  };

  return (
    <Map
      onLoad={(ymaps) => {
        loadSuggest(ymaps);
      }}
      modules={['SuggestView', 'control.SearchControl', 'geocode']}
      onClick={handleMapClick}
      className="map"
      state={{
        center: coordinates,
        zoom: 15
      }}>
      <ZoomControl />
      <Placemark geometry={coordinates} />
    </Map>
  );
});

export default Location;
