import { FC } from 'react';
import Location from './Location/Location';
import { YANDEX_API_KEY } from '@config';
import './Geolocation.scss';
import { YMaps } from '@pbe/react-yandex-maps';
import GeoInput from './GeoInput/GeoInput';
import Typography from 'components/ui-kit/Typography/Typography';
import { ChangeProductDataType, ProductDataInterface } from '../types';

interface GeolocationProps {
  changeOrderData: ChangeProductDataType;
  coordinates: ProductDataInterface['coordinates'];
  location: ProductDataInterface['location'];
}

const Geolocation: FC<GeolocationProps> = ({ changeOrderData, location, coordinates }) => {
  console.log(coordinates);
  return (
    <div className="map__wrapper">
      <YMaps enterprise query={{ apikey: YANDEX_API_KEY }}>
        <Location changeOrderData={changeOrderData} coordinates={coordinates} />
        <div className="search__container">
          <Typography variant="span" fontWeight="semi-bold" fontSize={24}>
            {location}
          </Typography>
          <GeoInput />
        </div>
      </YMaps>
    </div>
  );
};

export default Geolocation;
