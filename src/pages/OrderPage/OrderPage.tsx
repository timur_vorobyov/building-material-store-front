// import { useAuth } from '@contexts/auth/use-auth-hook';
import { useAuth } from '@contexts/auth/use-auth-hook';
import { useEffect, useState } from 'react';
import DesctopOrderPage from './DesctopOrderPage/DesctopOrderPage';
import { StageNumbersEnum } from './enums';
import MobileOrderPage from './MobileOrderPage/MobileOrderPage';
import OrderStage from './OrderStage/OrderStage';
import ProductsListStage from './ProductsListStage/ProductsListStage';
import './OrderPage.scss';
import Navbar from 'components/Navbar/Navbar';
import NoDataFound from 'components/NoDataFound/NoDataFound';

const OrderPage = () => {
  const [totalPrice, setTotalPrice] = useState(0);
  const [currentStage, setCurrentStage] = useState<number>(StageNumbersEnum.Cart);
  const { cart } = useAuth();

  useEffect(() => {
    let fullPrice = 0;
    cart.forEach((c) => {
      fullPrice = fullPrice + c.count * c.product.price;
    });

    setTotalPrice(fullPrice);
  }, [cart]);

  const stages: {
    [key: number]: JSX.Element;
  } = {
    [StageNumbersEnum.Cart]: <ProductsListStage />,
    [StageNumbersEnum.Order]: <OrderStage />
  };

  return (
    <>
      <Navbar />
      {cart.length ? (
        <main>
          <DesctopOrderPage
            currentStage={currentStage}
            setCurrentStage={setCurrentStage}
            stages={stages}
            totalPrice={totalPrice}
          />
          <MobileOrderPage
            currentStage={currentStage}
            setCurrentStage={setCurrentStage}
            stages={stages}
            totalPrice={totalPrice}
          />
        </main>
      ) : (
        <NoDataFound title="У вас нет товаров в корзине" />
      )}
    </>
  );
};

export default OrderPage;
