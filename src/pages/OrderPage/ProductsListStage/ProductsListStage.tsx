import DesctopProductsListStage from './DesctopProductsListStage/DesctopProductsListStage';
import MobileProductsListStage from './MobileProductsListStage/MobileProductsListStage';
import './ProductsListStage.scss';

const ProductsListStage = () => {
  return (
    <>
      <DesctopProductsListStage />
      <MobileProductsListStage />
    </>
  );
};

export default ProductsListStage;
