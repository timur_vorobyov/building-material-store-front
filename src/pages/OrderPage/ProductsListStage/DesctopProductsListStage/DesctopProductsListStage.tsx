import { APP_MEDIA_URL } from '@config';
import { useAuth } from '@contexts/auth/use-auth-hook';
import { useLocale } from '@contexts/locale/use-locale-hook';
import { formatPrice } from '@helpers/text-fromatter.helper';
import { ProductInterface } from '@services/product/interface/product.interface';
import { CartInterface } from '@services/product/product-cart/interface/product-cart.interface';
import { useProductService } from '@services/product/use-product-service-hook';
import CartController from 'components/CartController/CartController';
import Typography from 'components/ui-kit/Typography/Typography';
import { LocalesEnum } from 'i18n';
import translate from 'i18n/translate';
import { useQuery } from 'react-query';
import './DesctopProductsListStage.scss';

const DesctopProductsListStage = () => {
  const { user, cart } = useAuth();
  const { locale } = useLocale();
  const service = useProductService();
  const { data } = useQuery({
    queryKey: ['carts'],
    queryFn: () => service.listFromCart({ userId: user!.id }),
    initialData: []
  });

  const carts = data as CartInterface[];

  const getName = (product: ProductInterface) => {
    const names = {
      [LocalesEnum.Russian]: product.nameRu,
      [LocalesEnum.Uzbek]: product.nameUz,
      [LocalesEnum.English]: product.nameEn
    };

    return names[locale];
  };

  return (
    <div className="desctop-products-list-stage__wraper">
      <div className="header">
        <Typography variant="span" fontWeight="semi-bold">
          {translate('product')}
        </Typography>
        <Typography variant="span" fontWeight="semi-bold">
          {translate('amount')}
        </Typography>
        <Typography variant="span" fontWeight="semi-bold">
          {translate('sum')}
        </Typography>
      </div>
      <div>
        {carts.map(({ product }, index: number) => {
          const cartItemIndex = cart.findIndex((item) => item.product.id === product.id);

          return (
            <div key={index} className="desctop-products-list-stage">
              <div className="descriptions">
                <img className="img" src={APP_MEDIA_URL + product.imagesUrls[0]} />
                <div className="info">
                  <div className="name">
                    <a href={`/product/${product.id}`}>
                      <Typography variant="span">{getName(product)}</Typography>
                    </a>
                  </div>
                  <div className="price">
                    <Typography variant="span" fontWeight="semi-bold">
                      <>
                        {formatPrice(product.price) + ' '}
                        UZS
                      </>
                    </Typography>
                  </div>
                </div>
              </div>
              <div className="amount">
                <CartController product={product} />
              </div>
              <Typography variant="span" fontWeight="semi-bold">
                {formatPrice(cart[cartItemIndex].count * product.price)} UZS
              </Typography>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default DesctopProductsListStage;
