import { useAuth } from '@contexts/auth/use-auth-hook';
import { useProductService } from '@services/product/use-product-service-hook';
import Navbar from 'components/Navbar/Navbar';
import NoDataFound from 'components/NoDataFound/NoDataFound';
import ProductsList from 'components/ProductsList/ProductsList';
import { useEffect } from 'react';
import { useQuery } from 'react-query';

const FavoriteProductPage = () => {
  const { user, favorite } = useAuth();
  const service = useProductService();
  const { data, isFetching, refetch } = useQuery({
    queryKey: ['favoriteProducts', { userId: user!.id }],
    queryFn: () => service.listFavorite({ userId: user!.id }),
    initialData: []
  });

  useEffect(() => {
    refetch();
  }, [favorite]);

  if (isFetching) return <></>;

  return (
    <>
      <Navbar />
      {data?.length ? (
        <main>
          <ProductsList isFetching={isFetching} products={data} />
        </main>
      ) : (
        <NoDataFound title="У вас нет товаров в избранных" />
      )}
    </>
  );
};

export default FavoriteProductPage;
