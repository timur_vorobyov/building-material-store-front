import './ProductPage.scss';
import Container from 'components/Container/Container';
import { useParams } from 'react-router-dom';
import { useQuery } from 'react-query';
import { useProductService } from '@services/product/use-product-service-hook';
import { APP_MEDIA_URL } from '@config';
import Navbar from 'components/Navbar/Navbar';
import Heading from 'components/Heading/Heading';
import { CiWallet } from 'react-icons/ci';
import { BsTruck } from 'react-icons/bs';
import { HiOutlineShoppingBag } from 'react-icons/hi';
import PaymentImage from './assets/images/payment-product.png';
import Typography from 'components/ui-kit/Typography/Typography';
import { formatPrice } from '@helpers/text-fromatter.helper';
import CartController from 'components/CartController/CartController';

const ProductPage = () => {
  const params = useParams();
  const service = useProductService();
  const { data } = useQuery({
    queryKey: ['product'],
    queryFn: () => service.single(params.id)
  });
  const product = data?.data;

  if (!product) return <></>;

  return (
    <>
      <Navbar />

      <Container>
        <div className="listing-client">
          <div className="listing-client-container">
            <Heading title={product.nameRu} />
            <div className="listing-info-wrapper">
              <img src={APP_MEDIA_URL + product.imagesUrls[0]} className="image" alt="Image" />
              <div className="general-info-wrapper">
                <div className="general-info">
                  <div className="general-info__brand">
                    <Typography variant="span">Бренд: </Typography>
                    <Typography variant="span">{product.brand.name}</Typography>
                  </div>
                  <div className="general-info__category">
                    <Typography variant="span">Категория: </Typography>
                    <Typography variant="span">{product.category.nameRu}</Typography>
                  </div>
                  <div className="general-info__desc">
                    <Typography variant="span">
                      Предназначается для окраски металлических, деревянных и других поверхностей,
                      подвергающихся атмосферным воздействиям, и для окраски наружный и внутри
                      помещений. Эмаль обладает высокими защитными, декоративными свойствами и
                      выпускается различных цветов.
                    </Typography>
                  </div>
                  <div className="general-info__price">
                    <Typography variant="span">{formatPrice(product.price)} сум</Typography>
                  </div>
                </div>
                <CartController product={product} />
              </div>
              <div className="payment-ways">
                <div className="convinience">
                  <div className="convinience-item">
                    <CiWallet size={25} />
                    <span>Удобная оплата</span>
                  </div>
                  <div className="convinience-item">
                    <BsTruck size={25} />
                    <span>Быстрая доставка</span>
                  </div>
                  <div className="convinience-item">
                    <HiOutlineShoppingBag size={25} />
                    <span>Бережная упаковка</span>
                  </div>
                </div>
                <hr />
                <div className="payment">
                  <img src={PaymentImage} alt="Image" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default ProductPage;
