import { OrderInterface } from '@services/order/interface/order.interface';
import { FC } from 'react';
import './OrderStage.scss';
import Typography from 'components/ui-kit/Typography/Typography';
import { formatPrice } from '@helpers/text-fromatter.helper';
interface OrderStageProps {
  orders: OrderInterface[];
}

const OrderStage: FC<OrderStageProps> = ({ orders }) => {
  return (
    <>
      {orders.map((order) => {
        return (
          <div className="order-stage-2">
            <Typography variant="span">{order.id}</Typography>
            <Typography variant="span">{order.location}</Typography>
            <Typography variant="span">{formatPrice(order.fullPrice)}</Typography>
          </div>
        );
      })}
    </>
  );
};

export default OrderStage;
