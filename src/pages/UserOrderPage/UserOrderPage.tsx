import Navbar from 'components/Navbar/Navbar';
import Tabs from 'components/Tabs/Tabs';
import { useState } from 'react';
import { StageNumbersEnum } from './enums';
import OrderStage from './OrderStage/OrderStage';
import Container from 'components/Container/Container';
import './UserOrderPage.scss';
import { useQuery } from 'react-query';
import { useOrderService } from '@services/order/use-order-service-hook';
import { useAuth } from '@contexts/auth/use-auth-hook';
import Typography from 'components/ui-kit/Typography/Typography';

const UserOrderPage = () => {
  const [currentStage, setCurrentStage] = useState<number>(StageNumbersEnum.Pending);
  const { user } = useAuth();
  const service = useOrderService();

  const { data: ordersPending } = useQuery({
    queryKey: ['ordersPending'],
    queryFn: () => service.listOrders({ userId: user!.id, status: false }),
    initialData: []
  });

  const { data: ordersReady } = useQuery({
    queryKey: ['ordersReady'],
    queryFn: () => service.listOrders({ userId: user!.id, status: true }),
    initialData: []
  });

  if (!ordersPending || !ordersReady) return <></>;
  const stages: {
    [key: number]: JSX.Element;
  } = {
    [StageNumbersEnum.Pending]: <OrderStage orders={ordersPending} />,
    [StageNumbersEnum.Ready]: <OrderStage orders={ordersReady} />
  };

  return (
    <>
      <Navbar />

      <Container>
        <div className={'user-order'}>
          <Tabs
            currentStage={currentStage}
            setCurrentStage={setCurrentStage}
            firstTitle={<>В ожидании</>}
            secondTitle={<>Готовые</>}
          />
          <div className="header">
            <Typography variant="span" fontWeight="semi-bold">
              ID
            </Typography>
            <Typography variant="span" fontWeight="semi-bold">
              Место доставки
            </Typography>
            <Typography variant="span" fontWeight="semi-bold">
              Стоимость (сум)
            </Typography>
          </div>
          {stages[currentStage]}
        </div>
      </Container>
    </>
  );
};

export default UserOrderPage;
