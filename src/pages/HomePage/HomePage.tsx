import { FC, useEffect, useState } from 'react';
import { DEFAULTL_SKIP, DEFAULT_TAKE, INITIAL_FILTERS_VALUE } from './consts';
import { useQuery } from 'react-query';
import { useProductService } from '@services/product/use-product-service-hook';
import { ProductInterface } from '@services/product/interface/product.interface';
import { QueryProductDto } from '@services/product/dto/query-product.dto';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useLocation } from 'react-router-dom';
import { useAuthModal } from '@contexts/auth-modal/use-auth-modal-hook';
import Navbar from 'components/Navbar/Navbar';
import NoDataFound from 'components/NoDataFound/NoDataFound';
import ProductsList from '../../components/ProductsList/ProductsList';

const HomePage: FC = () => {
  const location = useLocation();
  const { setAuthModalData } = useAuthModal();
  const [isModalShown, setIsModalShown] = useState(false);
  const [filters, setFilters] = useState<QueryProductDto>(INITIAL_FILTERS_VALUE);
  const service = useProductService();
  const { data, isFetching } = useQuery({
    queryKey: ['products', filters],
    queryFn: () => service.list(filters),
    initialData: [[], 0]
  });
  const [productsList, total] = data as [ProductInterface[], number];
  const [products, setAds] = useState(productsList);
  const [totalAds, setTotalAds] = useState(0);

  useEffect(() => {
    if (location.state?.isAuthModalShown) {
      setAuthModalData({ isShown: true, isSignUp: false });
    }
  }, []);

  useEffect(() => {
    if (filters.skip) {
      setAds([...products, ...productsList]);
    } else {
      setAds(productsList);
    }
  }, [productsList]);

  useEffect(() => {
    setTotalAds(total);
  }, [total]);

  // const applyFilters = (updatedFilters: QueryProductDto) => {
  //   setFilters({ ...updatedFilters, skip: DEFAULTL_SKIP });
  // };

  console.log(isModalShown, DEFAULTL_SKIP);
  const loadMore = () => {
    if (isFetching) return;
    setFilters({ ...filters, skip: Number(filters?.skip || 0) + DEFAULT_TAKE });
  };

  return (
    <>
      <Navbar handleModalShown={(value: boolean) => setIsModalShown(value)} />
      {products.length === 0 && filters !== INITIAL_FILTERS_VALUE ? (
        <NoDataFound
          title="Результат не найден"
          subtitle="Попробуйте подобрать другую комбинацию поиска"
        />
      ) : (
        <main>
          <InfiniteScroll
            dataLength={productsList.length}
            next={loadMore}
            hasMore={productsList.length < Number(totalAds)}
            loader={<></>}>
            {products && <ProductsList isFetching={isFetching} products={products} />}
          </InfiniteScroll>
        </main>
      )}
    </>
  );
};

export default HomePage;
