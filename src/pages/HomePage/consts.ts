import { QueryProductDto } from '@services/product/dto/query-product.dto';

export const MIN_PRICE = 1000;
export const MAX_PRICE = 1000000;

export const DEFAULT_TAKE = 12;
export const DEFAULTL_SKIP = 0;

export const INITIAL_BATHROOM_NUMBER = 0;
export const INITIAL_LIVINGROOM_NUMBER = 0;
export const INITIAL_BED_NUMBER = 0;
export const INITIAL_TOILET_NUMBER = 0;
export const INITIAL_GUESTS_NUMBER = 0;

export const INITIAL_FILTERS_VALUE: QueryProductDto = {
  take: DEFAULT_TAKE,
  skip: DEFAULTL_SKIP,
  price: [MIN_PRICE, MAX_PRICE]
};
