interface AdFeaturesInterface {
  needed: number[];
  common: number[];
  safety: number[];
  specific: number[];
}

export type FiltersAllowedValues = string | number | number[] | AdFeaturesInterface;

export type ISliderEvent = React.ChangeEvent<Record<string, unknown>> | undefined;
