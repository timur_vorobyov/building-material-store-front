export function replacePathParams(
  path: string,
  params: { [key: string]: string | number },
  prefix = ':'
) {
  let newPath = path;

  Object.entries(params).forEach(([key, value]) => {
    newPath = newPath.replace(prefix + key, String(value));
  });
  return newPath;
}
