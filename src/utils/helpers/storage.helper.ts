import { Cookies } from 'react-cookie';
import dayjs from 'dayjs';
import hmacSha256 from 'crypto-js/hmac-sha256';
import base64 from 'crypto-js/enc-base64';

interface CookieSetOptions {
  path?: string;
  expires?: Date;
  maxAge?: number;
  domain?: string;
  secure?: boolean;
  httpOnly?: boolean;
  sameSite?: boolean | 'none' | 'lax' | 'strict';
  encode?: (value: string) => string;
}

const getParams = (): CookieSetOptions => ({
  domain: '',
  path: '/',
  sameSite: false,
  httpOnly: false,
  expires: dayjs().add(1, 'week').toDate()
});

export function getLocalStorage(name: string): string | object | undefined {
  const res = window.localStorage.getItem(name);
  if (res === null) return undefined;

  try {
    return JSON.parse(String(res));
  } catch (e) {
    return res;
  }
}

export function setLocalStorage(name: string, value: string) {
  return window.localStorage.setItem(name, value);
}

//research
export function signCookie(val: string, secret: string) {
  if (null == secret) throw new TypeError('Secret key must be provided.');
  return val + '.' + base64.stringify(hmacSha256(val, secret));
}
//research
export function unSignCookie(input: string, secret: string): string {
  const val = input.slice(0, input.lastIndexOf('.'));

  const tentativeValue = val.slice(2, val.length),
    expectedInput = signCookie(val, secret),
    expectedBuffer = Buffer.from(expectedInput),
    inputBuffer = Buffer.from(input);

  return expectedBuffer.length === inputBuffer.length && Buffer.compare(expectedBuffer, inputBuffer)
    ? tentativeValue
    : '';
}

export function getCookie(name: string): string | object | undefined {
  const cookiesIns = new Cookies();

  try {
    return cookiesIns.get(name);
  } catch {
    return undefined;
  }
}

export function deleteLocalStorage(name: string) {
  return window.localStorage.removeItem(name);
}

export function deleteCookie(name: string) {
  const cookiesIns = new Cookies();
  return cookiesIns.remove(name, getParams());
}
