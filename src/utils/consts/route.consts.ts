export const routes = {
  home: '/',
  login: '/login',
  product: '/product/:id',
  createProduct: '/product/create',
  order: '/order/create',
  favoriteProduct: '/product/favorite',
  myProductsList: '/product/owner-list',
  userOrder: '/orders'
};
