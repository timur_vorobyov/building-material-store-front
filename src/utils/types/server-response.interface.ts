export default interface ServerResponseInterface<I> {
  message?: string;
  data: I;
  status: string;
  total?: number;
}
