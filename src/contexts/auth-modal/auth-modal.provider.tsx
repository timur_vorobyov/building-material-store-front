import { useState } from 'react';
import { FC, PropsWithChildren } from 'react';
import AuthModalContext from './auth-modal.context';
import { INITIAL_AUTH_MODAL_VALUE } from './consts';

const AuthModalContextProvider: FC<PropsWithChildren> = ({ children }) => {
  const [authModalData, setAuthModalData] = useState(INITIAL_AUTH_MODAL_VALUE);
  const value = { authModalData, setAuthModalData };

  return <AuthModalContext.Provider value={value}>{children}</AuthModalContext.Provider>;
};

export default AuthModalContextProvider;
