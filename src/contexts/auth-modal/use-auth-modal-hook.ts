import { useContext } from 'react';
import AuthModalContext from './auth-modal.context';
import AuthModalContextInterface from './auth-modal.context.interface';

export const useAuthModal = () => {
  const context = useContext<AuthModalContextInterface>(AuthModalContext);
  if (context === undefined) {
    throw new Error(
      'AuthModalContext was not provided. Make sure your component is a part of AuthModalProvider'
    );
  }

  return context;
};
