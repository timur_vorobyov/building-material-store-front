import { Context, createContext } from 'react';
import AuthModalContextInterface from './auth-modal.context.interface';

const AuthModalContext: Context<AuthModalContextInterface> =
  createContext<AuthModalContextInterface>({} as AuthModalContextInterface);

export default AuthModalContext;
