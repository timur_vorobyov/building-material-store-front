export default interface AuthModalContextInterface {
  authModalData: {
    isShown: boolean;
    isSignUp: boolean;
  };
  setAuthModalData: (authModalData: { isShown: boolean; isSignUp: boolean }) => void;
}
