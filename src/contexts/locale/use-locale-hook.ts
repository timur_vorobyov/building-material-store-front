import { useContext } from 'react';
import { LocaleContextInterface } from './locale.context.interface';
import LocaleContext from './locale.context';

export const useLocale = () => {
  const context = useContext<LocaleContextInterface>(LocaleContext);
  if (Object.keys(context).length === 0) {
    throw new Error(
      'LocaleContext was not provided. Make sure your component is a part of LocaleProvider'
    );
  }
  return context;
};
