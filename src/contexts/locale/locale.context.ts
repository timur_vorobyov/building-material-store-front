import { Context, createContext } from 'react';
import { LocaleContextInterface } from './locale.context.interface';

const LocaleContext: Context<LocaleContextInterface> = createContext<LocaleContextInterface>(
  {} as LocaleContextInterface
);

export default LocaleContext;
