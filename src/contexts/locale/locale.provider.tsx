import { FC, PropsWithChildren, useState } from 'react';
import { getLocalStorage, setLocalStorage } from '@helpers/storage.helper';
import LocaleContext from './locale.context';
import { LocalesEnum } from 'i18n';

const LocaleContextProvider: FC<PropsWithChildren> = ({ children }) => {
  let defaultLocale = LocalesEnum.English;
  const localStorageLocale = getLocalStorage('locale');
  if (localStorageLocale) {
    defaultLocale = localStorageLocale as LocalesEnum;
  }
  const [locale, setLocale] = useState<LocalesEnum>(defaultLocale);

  const setLocaleData = (value: LocalesEnum) => {
    setLocalStorage('locale', value);
    setLocale(value);
  };

  const value = {
    locale,
    setLocaleData
  };
  return <LocaleContext.Provider value={value}>{children}</LocaleContext.Provider>;
};

export default LocaleContextProvider;
