import { LocalesEnum } from 'i18n';

export interface LocaleContextInterface {
  locale: LocalesEnum;
  setLocaleData: (locale: LocalesEnum) => void;
}
