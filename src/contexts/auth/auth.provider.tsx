import { deleteLocalStorage } from '@helpers/storage.helper';
import {
  getCookieUserHelper,
  getUserDataHelper,
  resetAuthHelper,
  setUserDataHelper
} from '@services/auth/auth.helper';
import { CartInterface } from '@services/product/product-cart/interface/product-cart.interface';
import UserInterface from '@services/user/interface/user.interface';
import { useUserService } from '@services/user/use-user-service-hook';
import { FC, PropsWithChildren, useEffect, useState } from 'react';
import AuthContext from './auth.context';

const AuthContextProvider: FC<PropsWithChildren> = ({ children }) => {
  const userService = useUserService();
  const [user, setUser] = useState<UserInterface>();
  const [favorite, setFavorite] = useState<number[]>([]);
  const [cart, setCart] = useState<CartInterface[]>([]);
  const [loaded, setLoaded] = useState<boolean>(false);

  const logOut = async () => {
    deleteLocalStorage('user');
    setUser(undefined);
    await resetAuthHelper();
  };

  const updateAuthData = async (data: UserInterface) => {
    try {
      const adsIds = data.favorite.map((ad: { id: number }) => ad.id);
      setFavorite(adsIds);
      setCart(data.cart);
      setUser(data);
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    if (!user) return;
    setUserDataHelper(user);
    setLoaded(true);
  }, [user]);

  useEffect(() => {
    const uCookieData = getCookieUserHelper();
    //research
    if (!uCookieData) {
      logOut();
      setLoaded(true);
      return;
    }

    const { id } = uCookieData;

    //research
    if (!uCookieData || typeof id === 'undefined') {
      setLoaded(true);
      return;
    }

    const userData = getUserDataHelper();

    if (typeof userData !== 'undefined' && userData.id !== id) {
      setLoaded(true);
      return;
    }

    try {
      userService.single(id).then((profile) => {
        const adsIds = profile.data?.favorite.map((product) => product.id) || [];
        setFavorite(adsIds);
        setUser(profile.data);
        setCart(profile.data.cart);
        setLoaded(true);
      });
      return;
    } catch (e) {
      console.log(e);
    }
  }, []);

  const value = {
    user,
    cart,
    setCart,
    logOut,
    setUser,
    updateAuthData,
    favorite,
    setFavorite
  };

  if (loaded) {
    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
  }

  return null;
};

export default AuthContextProvider;
