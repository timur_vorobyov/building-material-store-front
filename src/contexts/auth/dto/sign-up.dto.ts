export default interface SignUpDto {
  name: string;
  password: string;
  phone: string;
}
