export default interface LoginDto {
  password: string;
  phone: string;
}
