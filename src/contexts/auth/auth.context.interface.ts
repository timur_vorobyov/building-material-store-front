import { CartInterface } from '@services/product/product-cart/interface/product-cart.interface';
import UserInterface from '@services/user/interface/user.interface';

export default interface AuthContextInterface {
  cart: CartInterface[];
  setCart: (cart: CartInterface[]) => void;
  user: UserInterface | undefined;
  logOut: () => Promise<void>;
  updateAuthData: (data: UserInterface) => Promise<void>;
  setUser: (user: UserInterface) => void;
  favorite: number[];
  setFavorite: (favorite: number[]) => void;
}
