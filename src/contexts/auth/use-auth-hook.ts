import { useContext } from 'react';
import AuthContext from './auth.context';
import AuthContextInterface from './auth.context.interface';

export const useAuth = () => {
  const context = useContext<AuthContextInterface>(AuthContext);
  if (context === undefined) {
    throw new Error(
      'AuthContext was not provided. Make sure your component is a part of AuthProvider'
    );
  }

  return context;
};
