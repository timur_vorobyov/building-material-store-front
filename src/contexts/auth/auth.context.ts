import AuthContextInterface from '@contexts/auth/auth.context.interface';
import { Context, createContext } from 'react';

const AuthContext: Context<AuthContextInterface> = createContext<AuthContextInterface>(
  {} as AuthContextInterface
);

export default AuthContext;
