#!/bin/bash

echo "Deployment started"
su - ubuntu -c "cd /home/ubuntu/app && git pull"

echo "Running app...."
sudo docker-compose up -d --build front

echo "Cleaning up...."
sudo docker system prune -af --volumes

echo "Done"
